// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: true,
  // apiBaseUrl: 'http://192.168.3.94/anhaj/api/', //localhost
  // baseUrl: 'http://192.168.3.94/anhaj/webroot/img/vehicleimages/'
  apiBaseUrl: 'http://184.72.171.234/anhajver9/api/', //testing serve
  baseUrl: 'http://184.72.171.234/anhajver9/webroot/img/vehicleimages/'
};
