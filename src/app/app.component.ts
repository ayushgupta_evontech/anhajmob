import { Component } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { CookieService } from 'ngx-cookie-service';
import { routerTransition } from './shared/animation/animation';

declare var navigator: any;
declare var StatusBar: any;
declare var device: any;

@Component({
	selector: 'app-root',
	templateUrl: './app.component.html',
	animations: [ routerTransition ],
	styleUrls: ['./app.component.css']
})
export class AppComponent {
	title = 'app';
	showFooter=false;
	constructor(private _cookieService:CookieService,private translate: TranslateService) {
		translate.addLangs(['en', 'ar']);
		translate.setDefaultLang('en');
		let self=this;
		function deviceReady(){
			document.removeEventListener('deviceready', deviceReady, false);                     
			setTimeout(function() {
				navigator.splashscreen.hide();
			}, 2000);
			
			// setTimeout(function() {
				// if(device.platform == 'Android' && localStorage.getItem('userType') == 'employee'){
				// 	StatusBar.backgroundColorByHexString("#2F1623");
				// } else {
				// 	StatusBar.backgroundColorByHexString("#032E37");
				// };             
			// }, 5000);				
		}
		document.addEventListener("deviceready", deviceReady, false);	   
	}

	getState(outlet) {
		this.showFooter=true;
		return outlet.activatedRouteData.state;
	}
}
