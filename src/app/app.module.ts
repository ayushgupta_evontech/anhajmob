import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { appRouter } from './app.router';
import { AppComponent } from './app.component';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClient, HttpClientModule, HttpClientJsonpModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { CookieService } from 'ngx-cookie-service';
import { FlashMessagesModule } from 'ngx-flash-messages';
import { SlickCarouselModule } from 'ngx-slick-carousel';

import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';

import { AuthGuard } from './shared/guard/auth.guard';
import { HeaderComponent } from './components/elements/header/header.component';
import { FooterComponent } from './components/elements/footer/footer.component';
import { SignInComponent } from "./components/auth/sign-in/sign-in.component";
import { ForgotPasswordComponent } from './components/auth/forgot-password/forgot-password.component';
import { OtpVerificationComponent } from './components/auth/otp-verification/otp-verification.component';
import { ChangePasswordComponent } from './components/auth/change-password/change-password.component';
import { ResetPasswordComponent } from './components/auth/reset-password/reset-password.component';
import { RegisterComponent } from './components/auth/register/register.component';
import { EditProfileComponent } from './components/edit-profile/edit-profile.component';
import { TestDriveBookingsComponent } from './components/elements/testdrive-bookings/testdrive-bookings.component';

import { DataStorageServices } from './shared/services/dataStorage.service';

import { SellcarResolver } from './shared/resolver/sellcar-resolver.service';
import { SearchResolver } from './components/listing/resolver/search-resolver.service';
import { HttpServiceInterceptor } from "./shared/interceptor/http-service-interceptor";
import { TranslateService } from '@ngx-translate/core';

export function httploaderFactory(httpClient: HttpClient): TranslateHttpLoader{
	return new TranslateHttpLoader(httpClient, './assets/languages/','.json');
}

@NgModule({
	declarations: [
		AppComponent,
		HeaderComponent,
		FooterComponent,
		SignInComponent,
		RegisterComponent,
		ForgotPasswordComponent,
		OtpVerificationComponent,
		ChangePasswordComponent,
		ResetPasswordComponent,
		EditProfileComponent,
		TestDriveBookingsComponent
	],
	imports: [
		appRouter,
		FormsModule,	
		CommonModule,
		BrowserModule,
		HttpClientModule,
		HttpClientJsonpModule,
		BrowserAnimationsModule,
		ReactiveFormsModule,
		FlashMessagesModule,
		TranslateModule.forRoot({
			loader: {
				provide: TranslateLoader,
				useFactory: httploaderFactory,
				deps: [HttpClient]
			}
		})
	],

	providers: [
		SellcarResolver,
		SearchResolver,
		CookieService,
		AuthGuard,
		DataStorageServices,
		SlickCarouselModule,
		HttpClient,
		{
			provide: HTTP_INTERCEPTORS,
			useClass: HttpServiceInterceptor,
			multi: true
 		}
	],
	bootstrap: [AppComponent]
})
export class AppModule { }
