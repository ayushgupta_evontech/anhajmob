import { Injectable } from '@angular/core'
import { HttpClient,HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Country } from '../models/country';
import { Request, Response, Headers, RequestOptions } from '@angular/http';

/**
 * Data storage services.
 * 
 * @export
 * @class DataStorageServices
 */
@Injectable()
export class DataStorageServices {
    countries:BehaviorSubject<any> = new BehaviorSubject(null);
    editUser:BehaviorSubject<any> = new BehaviorSubject(null);	
    
    constructor(private http: HttpClient) {}

    public getCityAPI(id:any): Observable<any> {              
        return this.http.get('users/app_getAllCities/'+id);
    }  

    public getCountryAPI(): Observable<any> {              
        return this.http.get('users/app_getAllCountries');
    } 

    public getModelAPI(data:FormData): Observable<any> {              
        return this.http.post('Specifications/app_getAllModels',data);
    } 

    public getUserModelAPI(data:FormData): Observable<any> {              
        return this.http.post('vehicles/app_getAllUsersModels',data);
    } 

    public getVehicleinteriorAPI(): Observable<any> {              
        return this.http.get('vehicles/app_getAllVehicleInteriors');
    }

    public getVehicleseatAPI(): Observable<any> {              
        return this.http.get('vehicles/app_getAllVehicleSeats');
    }

    public getConditionAPI(): Observable<any> {              
        return this.http.get('vehicles/app_getAllConditions');
    }

    public getVehiclekmAPI(): Observable<any> {              
        return this.http.get('vehicles/app_getAllVehicleKms');
    }

    public getMakeAPI(data:FormData): Observable<any> {
        return this.http.post('Specifications/app_getAllMakes', data);
    }
    
    public getTrimsAPI(data:FormData): Observable<any> {
        return this.http.post('Specifications/app_getAllTrims',data);
    }

    public getColorAPI(): Observable<any> {              
        return this.http.get('vehicles/app_getAllColors');
    }

    public getSpecification(formData:FormData): Observable<any> {              
        return this.http.post('Specifications/app_getSpecificationData', formData);
    }
}