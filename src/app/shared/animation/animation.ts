import { trigger, style, transition, animate, group, animateChild, keyframes, query, stagger } from '@angular/animations';

export const animateList = [
	trigger('listAnimation', [
		transition('* => *', [

			query(':enter', style({ opacity: 0 }), { optional: true }),

			query(':enter', stagger('5ms', [
				animate('1s ease-in', keyframes([
					style({ opacity: 0, transform: 'translateY(-75%)', offset: 0 }),
					style({ opacity: .5, transform: 'translateY(35px)', offset: 0.3 }),
					style({ opacity: 1, transform: 'translateY(0)', offset: 1.0 }),
				]))]), { optional: true }),

			query(':leave', stagger('5ms', [
				animate('1s ease-in', keyframes([
					style({ opacity: 1, transform: 'translateY(0)', offset: 0 }),
					style({ opacity: .5, transform: 'translateY(35px)', offset: 0.3 }),
					style({ opacity: 0, transform: 'translateY(-75%)', offset: 1.0 }),
				]))]), { optional: true }),
		])
	]),
	trigger('explainerAnim', [
		transition('* => *', [
			query('.coll', style({ opacity: 0, transform: 'translateX(-40px)' })),

			query('.coll', stagger('50ms', [
				animate('40ms 1.2s ease-out', style({ opacity: 1, transform: 'translateX(0)' })),
			])),

			query('.coll', [
				animate(1000, style('*'))
			])

		])
	])
];

export const routerTransition = trigger('routerTransition', [
	transition('* <=> *', [
	  /* order */
	  /* 1 */ query(':enter, :leave', style({ position: 'fixed', width:'100%' })
		, { optional: true }),
	  /* 2 */ group([  // block executes in parallel
		query(':enter', [
		  style({ transform: 'translateX(100%)' }),
		  animate('0.5s ease-in-out', style({ transform: 'translateX(0%)' }))
		], { optional: true }),
		query(':leave', [
		  style({ transform: 'translateX(0%)' }),
		  animate('0.5s ease-in-out', style({ transform: 'translateX(-100%)' }))
		], { optional: true }),
	  ])
	])
  ])