import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FiltersearchComponent } from "../../components/filtersearch/filtersearch.component";
import { ExcerptFilter } from '../../shared/pipes/excerpt.pipe';
import { ObjNgForPipe } from '../../shared/pipes/ObjNgFor.pipe';
import { YearArray } from '../../shared/pipes/yearArray.pipe';

import { Ng5SliderModule } from 'ng5-slider';
import { NgxPaginationModule } from 'ngx-pagination';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
    imports: [
        CommonModule,
		FormsModule,
        ReactiveFormsModule,
        Ng5SliderModule, 
        NgxPaginationModule,
        TranslateModule
     ],
    declarations: [
         FiltersearchComponent,
         ExcerptFilter,
         ObjNgForPipe,
         YearArray
    ],
    exports: [
        FiltersearchComponent,
        ExcerptFilter,
        ObjNgForPipe,
        YearArray
    ]
})
export class SharedModule { }
