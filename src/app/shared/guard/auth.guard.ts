﻿import { Injectable, EventEmitter, Output } from '@angular/core';
import { Router, 
		ActivatedRoute, 
		CanActivate, 
		ActivatedRouteSnapshot,
		RouterStateSnapshot, 
		CanActivateChild,
		NavigationExtras
} from '@angular/router';
import { CookieService } from 'ngx-cookie-service';

/**
 * Auth Guard Service to check user access to components.
 * 
 * @export
 * @class AuthGuard
 * @implements {CanActivate}
 */
@Injectable()
export class AuthGuard implements CanActivate {

	constructor(private activatedRoute: ActivatedRoute, 
				private router: Router, 
				private _cookieService: CookieService) {}

	canActivate(route: ActivatedRouteSnapshot, 
				state: RouterStateSnapshot): boolean {
			//let url: string = state.url;
			if (this._cookieService.get('userInfo')) { 
				// logged in so return true
				return true;
			}else{
				this.router.navigate(['/']);
				return false;
			}
	}
}