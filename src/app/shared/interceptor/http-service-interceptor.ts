import { Injectable, Inject, Injector } from "@angular/core";
import { HttpEvent, HttpInterceptor, HttpHandler, HttpRequest, HttpResponse, HttpErrorResponse, HttpHeaders } from "@angular/common/http";
import { DOCUMENT } from "@angular/common";
import { Observable } from "rxjs/Observable";
import "rxjs/add/operator/do";
import { FlashMessagesService } from 'ngx-flash-messages';

import { _throw } from 'rxjs/observable/throw';
import { map, catchError, mergeMap } from 'rxjs/operators';
import { CookieService } from 'ngx-cookie-service';
import { environment } from "../../../environments/environment";
import { Router } from '@angular/router';

import { TranslateService } from '@ngx-translate/core';
import { inject } from "@angular/core/testing";
import { DataStorageServices } from '../../shared/services/dataStorage.service';

/**
 * http service interceptor to intercept all http requests.
 * 
 * @export
 * @class HttpServiceInterceptor
 * @implements {HttpInterceptor}
 */
@Injectable()
export class HttpServiceInterceptor implements HttpInterceptor {
    
    apiUrl: string;
    constructor(private _cookieService:CookieService,
                private route:Router,
                private injector:Injector,
                private flashMessagesService: FlashMessagesService) {
                    this.apiUrl = environment.apiBaseUrl;
    }

	intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        
        let reqUrl=req.url.split('/');
        if(reqUrl[1]=='assets'){
            const httpRequest = new HttpRequest(<any>req.method, req.url, req.body);
            req = Object.assign(req, httpRequest);                    
        }
        else if(reqUrl[2]=='vpic.nhtsa.dot.gov'){
            const httpRequest = new HttpRequest(<any>req.method, req.url, req.body);
            req = Object.assign(req, httpRequest);        
        }
        else{
            const httpRequest = new HttpRequest(<any>req.method, this.apiUrl+req.url, req.body);
            req = Object.assign(req, httpRequest);        
            const token: string=this._cookieService.get('token');
            if (token!=null) {
                req = req.clone({ headers: req.headers.set('Authorization', token) });
            }       
            req = req.clone({ headers: req.headers.set('Accept', 'application/json') });
        }
        		
		return next.handle(req).pipe(
			map((event: HttpEvent<any>) => {
                if (event instanceof HttpResponse) {
                    event.body.data = this.extractData(event);
                    let errorMsg:any;
                    if(event.body['status']==="success" && event.body['message']!=null){
                        errorMsg=this.injector.get(TranslateService).instant(event.body['message']);
                        this.flashMessagesService.show(errorMsg,{classes: ['alert', 'alert-success'], timeout: 5000});
                        window.scroll(0,0);
                    }else if(event.body['message']!=null){
                        errorMsg=this.injector.get(TranslateService).instant(event.body['message']);
                        this.flashMessagesService.show(errorMsg,{classes: ['alert', 'alert-danger'], timeout: 5000});
                        window.scroll(0,0);
                    }
                }
                return event;
            }),
			catchError((error: HttpErrorResponse) => {
                let data:any = this.handleError(error);
                // No valid access token provided.
                if(error.status == 401){
                    this._cookieService.deleteAll();
                    this.injector.get(DataStorageServices).editUser.next(null);
                    this.route.navigateByUrl('/home');
                } 
                this.flashMessagesService.show(data,{classes: ['alert', 'alert-danger'], timeout: 2200});               
                return _throw(error);
            })
		);
	}

    private extractData(res:any) {
            let body = res.body.data;
            return body || {};
    }

    private handleError(error: Response | any) {
        let errMsg: string;
        errMsg = error.error.message ? error.error.message : 'Something went wrong!';
        return errMsg;
    }
}