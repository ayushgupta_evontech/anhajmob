import { Pipe, PipeTransform } from '@angular/core';

/**
 * Excerpt Filter for the paragraph text.
 * 
 * @export
 * @class ExcerptFilter
 * @implements {PipeTransform}
 */
@Pipe({
	name: 'excerpt',
	pure: false
})
export class ExcerptFilter implements PipeTransform {
	transform(text: String, length: any, isExcerpt: boolean = true): any {
		if (isExcerpt) {
			if (!text || !length) {
				return text;
			}
			if (text.length > length) {
				return text.substr(0, length) + '...';
			}
		}
		return text;
	}
}