import { PipeTransform, Pipe } from '@angular/core';

/**
 * Obj NgFor Pipe to convert listing into object.
 * 
 * @export
 * @class ObjNgForPipe
 * @implements {PipeTransform}
 */
@Pipe({ name: 'ObjNgFor' })
export class ObjNgForPipe implements PipeTransform {

	transform(value): any {		
		let ObjData:any=[];
		ObjData=Object.keys(value).map(key => Object.assign({key : value[key], id : key}, ''));
		ObjData.sort(this.dynamicSort("key"));	
		return ObjData;
	}
	dynamicSort(property) {
		let sortOrder = 1;
	
		if(property[0] === "-") {
			sortOrder = -1;
			property = property.substr(1);
		}
	
		return function (a,b) {
			if(sortOrder == -1){
				return b[property].localeCompare(a[property]);
			}else{
				return a[property].localeCompare(b[property]);
			}        
		}
	}
}