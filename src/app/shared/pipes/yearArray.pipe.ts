import { Pipe, PipeTransform } from '@angular/core';

/**
 * Year array Pipe to add listing of upto years.
 * 
 * @export
 * @class YearArray
 * @implements {PipeTransform}
 */
@Pipe({
	name: 'yearArray',
	pure: false
})
export class YearArray implements PipeTransform {
	transform(year: any, range:number,operation: string='forth'): any {
		if (year && year!='null'){
			range=(range!=0)?range : 
					(year < new Date().getFullYear())? new Date().getFullYear()-parseInt(year) : 
																	parseInt(year)-new Date().getFullYear();
			if (operation == 'forth') {
				year = Array(++range).fill(1).map((x, i) =>  parseInt(year) + i);
			}else if (operation == 'back') {
				year = Array(++range).fill(1).map((x, i) => parseInt(year) - i);
			}
			return year;
		}
	}
}