import { Injectable, Inject } from "@angular/core";
import { Resolve,ActivatedRouteSnapshot,RouterStateSnapshot } from '@angular/router';
import { HttpClient,HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Rx';

/**
 * App resolver to get country list.
 * 
 * @export
 * @class AppResolver
 * @implements {Resolve}
 */
@Injectable()
export class AppResolver implements Resolve<any> {

    constructor(private http: HttpClient) {}

    resolve(
        route: ActivatedRouteSnapshot,
        rstate: RouterStateSnapshot
    ): Observable<any> {
        return this.http.get('users/app_getAllCountries');
    }
}