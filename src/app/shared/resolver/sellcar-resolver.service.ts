import { Injectable, Inject } from "@angular/core";
import { Resolve,ActivatedRouteSnapshot,RouterStateSnapshot } from '@angular/router';
import { HttpClient,HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Rx';
import 'rxjs/add/observable/forkJoin';
import { CookieService } from 'ngx-cookie-service';

/**
 * Sell car resolver get Makes, Conditions, Colors & vehicle limits.
 * 
 * @export
 * @class SearchResolver
 * @implements {Resolve}
 */
@Injectable()
export class SellcarResolver implements Resolve<any> {

    constructor(private http: HttpClient,
                private _cookieservice: CookieService) {}

    resolve(
        route: ActivatedRouteSnapshot,
        rstate: RouterStateSnapshot
    ): Observable<any> {
        let userInfo = JSON.parse(JSON.stringify(this._cookieservice.get('userInfo')));
        userInfo = JSON.parse(userInfo);
 
        let response2 = this.http.get('vehicles/app_getAllMakes');
        let response3 = this.http.get('vehicles/app_getAllConditions');
        let response4 = this.http.get('vehicles/app_getAllColors');
        let response5 = this.http.get('vehicles/app_getServerData');
        if (userInfo.usertype == 1) {
            let response6 = this.http.get('vehicles/app_getVehicleLimits');
            return Observable.forkJoin([response2, response3, response4, response5, response6]);
        } else {
            return Observable.forkJoin([response2, response3, response4, response5]);
        }         
    }
}