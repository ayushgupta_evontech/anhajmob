import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from './shared/guard/auth.guard';
import { SellcarResolver } from './shared/resolver/sellcar-resolver.service';
export const router: Routes = [
   {
        path: '',
        redirectTo: 'home',
        pathMatch: 'full'
    },{
        path: 'home',
        loadChildren: 'app/components/home/home.module#HomeModule',
        data: { state: 'home' } 
    },{
        path: 'userActivation',
        loadChildren: 'app/components/auth/userActivation/userActivation.module#UserActivationModule',
        data: { state: 'userActivation' }
    },{
        path: 'cms/:id',
        loadChildren: 'app/components/cms-pages/cms-pages.module#CMSPagesModule',
        data: { state: 'cms' }
    },{
        path: 'sellcar',
        loadChildren: 'app/components/sellcar/sellcar.module#SellcarModule',
        data: { state: 'sellcar' },
        resolve: { message: SellcarResolver },
        canActivate: [AuthGuard]
    },{
        path: 'listing',
        loadChildren: 'app/components/listing/listing.module#ListingModule',
        data: { state: 'listing' }
    },{
        path: 'detailresult/:id',
        loadChildren: 'app/components/detail-result/detail-result.module#DetailResultModule',
        data: { state: 'detailresult' }
    },{
        path: 'advance-search',
        loadChildren: 'app/components/advance-search/advance-search.module#AdvanceSearchModule',
        data: { state: 'advancesearch' }
    },{
        path: 'car-listing',
        loadChildren: 'app/components/car-listing/car-listing.module#CarListingModule',
        data: { state: 'carlisting' }
    },{
        path: '404',
        loadChildren: 'app/components/notfound/notfound.module#NotFoundModule'
    },{
        path: '**',
        redirectTo: '404'
    }
];

export const appRouter: ModuleWithProviders = RouterModule.forRoot(router);