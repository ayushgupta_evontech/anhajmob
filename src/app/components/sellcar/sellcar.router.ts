import { Routes, RouterModule } from '@angular/router';
import { SellcarComponent } from "./sellcar.component";

const SELLCAR_ROUTER: Routes = [
    {
        path: '',
        component: SellcarComponent
    }
];

export const sellcarRouter = RouterModule.forChild(SELLCAR_ROUTER);