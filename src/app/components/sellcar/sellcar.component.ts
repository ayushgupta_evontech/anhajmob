import { Component, OnInit, Input, Output, EventEmitter, ElementRef, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl, } from '@angular/forms';
import { NgForm } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { DataStorageServices } from '../../shared/services/dataStorage.service';
import { ObjNgForPipe } from '../../shared/pipes/ObjNgFor.pipe';
import { FlashMessagesService } from 'ngx-flash-messages';

import { SellcarServices } from './services/sellcar.service';
import { CookieService } from 'ngx-cookie-service';
import { TranslateService } from '@ngx-translate/core';
declare var window: any;

/**
 * Sell Car Page to Add vehicels.
 * 
 * @export
 * @class SellcarComponent
 * @implements {OnInit}
 */
@Component({
	selector: 'app-sellcar',
	templateUrl: './sellcar.component.html',
	styleUrls: ['./sellcar.component.css'],
	providers: [SellcarServices, ObjNgForPipe, CookieService]
})

export class SellcarComponent implements OnInit {
	countries: any;
	cities: any;
	makes: any;
	models: any;
	conditions: any;
	colors: any;
	documentsArray: any;
	files: any = [];
	isloader: boolean;
	addSellcarForm: FormGroup; //Form 
	imageLength: any;
	maxImageError: any;
	userInfo: any = [];
	vinInfo: any = {};
	carLimit: number;
	carCount: number;
	isShow: boolean = true;
	year_from: any=[];
	trims:any=[];
	modelBody:any=[];
	engineSize:any=[];
	engineCylinders:any=[];
	horsepower:any=[];
	fuelType:any=[];
	doors:any=[];
	seat:any=[];
	@ViewChild('vin') vin: ElementRef;

	constructor(
		private fb: FormBuilder,
		private route: ActivatedRoute,
		private router: Router,
		private translate:TranslateService,
		private ObjNgForPipe: ObjNgForPipe,
		private _cookieservice: CookieService,
		private datastorageServices: DataStorageServices,
		private flashMessagesService:FlashMessagesService,
		private sellcarServices: SellcarServices
	) {
		this.addSellcarForm = this.fb.group({
			'yearfrom': ['', Validators.required],
			'make_id': ['', Validators.required],
			'model_id': ['', Validators.required],
			'trim_id': ['', Validators.required],
			'model_body': [''],
			'engine_size': [''],
			'engine_cylinders' : [''],
			'horsepower': [''],
			'fuel_type': [''],
			'doors': [''],
			'country_id': ['', Validators.required],
			'city_id': ['', Validators.required],
			'price': ['', Validators.required],
			'kilometer': ['', Validators.required],
			'mileagetype': ['km', Validators.required],
			'condition_id': ['', Validators.required],
			'color_id': ['', Validators.required],
			'seat_id': ['', Validators.required],
			'vin': ['', Validators.required],
			'interior_id': [''],
			'video': [''],
			'description': [''],
			'fileInput': [''],
		});
		this.isloader = false;
		this.userInfo = JSON.parse(JSON.stringify(this._cookieservice.get('userInfo')));
		this.userInfo = JSON.parse(this.userInfo);

		let date: Date = new Date();
		// this.year_from = date.getFullYear();
	}
	
	ngOnInit() {
		this.datastorageServices.countries.subscribe(
			response => {
				this.countries = response;
			});
		this.route.data.subscribe(data => {
			//this.makes = this.ObjNgForPipe.transform(data.message[0]['data']);
			this.conditions = data.message[1]['data'];
			this.colors = data.message[2]['data'];
			
			this.modelBody = data.message[3]['data']['model_body'];
			this.engineSize = data.message[3]['data']['engine_size'];
			this.engineCylinders = data.message[3]['data']['engine_cylinders'];
			this.horsepower = data.message[3]['data']['horsepower'];
			this.fuelType = data.message[3]['data']['fuel_type'];
			this.fuelType = data.message[3]['data']['fuel_type'];
			this.doors= data.message[3]['data']['doors'];
			this.seat= data.message[3]['data']['seats'];
			this.year_from=data.message[3]['data']['years'];
			if (this.userInfo.usertype == 1) {
				this.carCount = data.message[4]['data']['is_count'];
				this.carLimit = data.message[4]['data']['car_limit'];
				this.isShow = (this.carCount < this.carLimit) ? true : false;
			}
		});
		this.vinInfo.year = '';
		this.vinInfo.make_id = '';
		this.vinInfo.model_id = '';
		this.vinInfo.trim_id = '';
		this.vinInfo.city_id = '';
		this.vinInfo.country_id = '';
		this.vinInfo.model_body = '';
		this.vinInfo.engine_size = '';
		this.vinInfo.engine_cylinders='';
		this.vinInfo.horsepower='';
		this.vinInfo.fuel_type='';
		this.vinInfo.doors='';
		this.vinInfo.seats='';
		if (this._cookieservice.get('userInfo')) {
			this.userInfo = JSON.parse(JSON.stringify(this._cookieservice.get('userInfo')));
			this.userInfo = JSON.parse(this._cookieservice.get('userInfo'));
			this.vinInfo.city_id = this.userInfo.city_id;
			this.vinInfo.country_id = this.userInfo.country_id;
			this.getAllCities(this.vinInfo.country_id);
		}

	}

	/**
	 * 
	 * @access public
	 * @param vin
	 * @desc function to vin information from the third party api.
	 */
	public getVinInfo(vin) {
		if (vin) {
			this.sellcarServices.getVinInfo(vin)
				.subscribe(
				data => {
					let err: any = data['Results'][0]['ErrorCode'].split(' ');
					this.vinInfo.year = '';
					this.vinInfo.make_id = '';
					this.vinInfo.model_id = '';
					this.vinInfo.description = '';
					this.vinInfo.price = '';
					this.vinInfo.seats='';
					if (err[0] == 0) {
						this.vinInfo.year=data['Results'][0]['ModelYear'];
						this.setMakes(this.vinInfo.year, data);
						let seatsIdx=this.seat.find(x => x.name == data['Results'][0]['seats']);
						(seatsIdx) ? this.vinInfo.seats = seatsIdx.id : ''; 
						this.vinInfo.price = data['Results'][0]['BasePrice'];
						let engineCylindersIdx=this.engineCylinders.find(x => x.name == data['Results'][0]['EngineCylinders']);
						let horsepowerIdx=this.horsepower.find(x => x.name == data['Results'][0]['EngineHP']);
						let fuelTypeIdx=this.fuelType.find(x => x.name==data['Results'][0]['FuelTypePrimary']);
						let doorsIdx=this.doors.find(x => x.name==data['Results'][0]['Doors']);

						this.vinInfo.engine_cylinders=(engineCylindersIdx)?engineCylindersIdx.id:'';
						this.vinInfo.horsepower=(horsepowerIdx)?horsepowerIdx.id:'';
						this.vinInfo.fuel_type=(fuelTypeIdx)?fuelTypeIdx.id:'';
						this.vinInfo.doors=(doorsIdx)?doorsIdx.id:'';
						// this.vinInfo.model_body=data['Results'][0][''];
						// this.vinInfo.engine_size=data['Results'][0][''];
						this.vinInfo.description = data['Results'][0]['OtherRestraintSystemInfo']
						this.vinInfo.country_id = (data['Results'][0]['Country']) ? data['Results'][0]['Country'] : this.vinInfo.country_id;
						this.getAllVinCities(this.vinInfo.country_id, data['Results'][0]['City']);
					}
				}
				)
		}
	}

	/**
	 * 
	 * @access public
	 * @param countryid
	 * @desc fetches all cities based on the country with its country id passed.
	 */
	public getAllCities(countryid) {
		if (countryid) {
			this.datastorageServices.getCityAPI(countryid)
				.subscribe(
				data => {
					this.cities = this.ObjNgForPipe.transform(data['data']);
				}
				)
		} else {
			this.cities = [];
			this.vinInfo.city_id = '';
		}
	}

	/**
	 * 
	 * @access public
	 * @param countryid
	 * @param data
	 * @desc fetches all cities based on the vin country with its country id passed.
	 */
	public getAllVinCities(countryid, data) {
		if (countryid && data) {
			this.datastorageServices.getCityAPI(countryid)
				.subscribe(
					data => {
						this.cities = this.ObjNgForPipe.transform(data['data']);
						this.vinInfo.city_id = data['Results'][0]['city'];
					}
				)
		} 
	}

	/**
	 * 
	 * @access public
	 * @param makeid 
	 * @desc fetches all Models based on the make with its make id passed
	 */
	public getAllModels(makeid:any, vinData:any) {
		this.vinInfo.trim_id='';
		if (makeid) {
			let yearFromIdx = this.year_from.find(x => x.name==this.vinInfo.year);				
			let formData=new FormData();
			formData.append('yearFrom' , yearFromIdx['id']);
			formData.append('make_id' , makeid);
			this.datastorageServices.getModelAPI(formData)
				.subscribe(
				data => {
					if(data.status=='success'){
						this.models=data['data'];
						if(vinData){
							let modelObj=data['data'].find(x => x.name.toLowerCase() == vinData['Results'][0]['Model'].toLowerCase());
							this.vinInfo.model_id = (modelObj)?modelObj.id:'';
							this.getAllTrims(this.vinInfo.model_id, vinData);
						}
					}else{
						this.models=[];
					}
				}
				)
		} else {
			this.models = [];
			this.vinInfo.model_id ='';
		}
	}

	/**
	 * 
	 * @desc As per the model id, get list of all trims.
	 * @access public
	 * @param model_id
	 */
	public getAllTrims(model_id:any, vinData:any){
		let yearFromIdx = this.year_from.find(x => x.name==this.vinInfo.year);				
		let formData=new FormData();
		formData.append('yearFrom' , yearFromIdx['id']);
		formData.append('make_id', this.vinInfo.make_id);
		formData.append('model_id', model_id);
		this.datastorageServices.getTrimsAPI(formData)
									.subscribe(
										data => {
											if(data.status=='success'){
												this.trims=data['data'];
												if(vinData){
													let trimObj=this.trims.find(x => x.name.toLowerCase()==vinData['Results'][0]['Trim'].toLowerCase());
													this.vinInfo.trim_id=(trimObj)?trimObj.id:'';
													// this.getSpecifications(this.vinInfo.trim_id);
												}
											}else{
												this.trims=[];
											}
										}
									);
	}
	
	/**
	 * 
	 * @desc As per the yera/make/model/trim get list of all other details.
	 * @access public
	 * @param trim_id
	 */
	public getSpecifications(trim_id: any){

		let yearFromIdx = this.year_from.find(x => x.name==this.vinInfo.year);				
		let formData=new FormData();
		formData.append('yearFrom' , yearFromIdx['id']);
		formData.append('make_id', this.vinInfo.make_id);
		formData.append('model_id', this.vinInfo.model_id);
		formData.append('trim_id', trim_id);
		this.datastorageServices.getSpecification(formData)
									.subscribe(
										response => {
											this.vinInfo.model_body='';
											this.vinInfo.engine_size='';
											this.vinInfo.engine_cylinders='';
											this.vinInfo.horsepower='';
											this.vinInfo.fuel_type='';
											this.vinInfo.doors='';
											this.vinInfo.seats='';
											if(response.status=='success'){
												this.vinInfo.model_body=response.data.model_body_id;
												this.vinInfo.engine_size=response.data.model_engine_cc_id;
												this.vinInfo.engine_cylinders=response.data.model_engine_cylinders_id;
												this.vinInfo.horsepower=response.data.model_engine_power_ps_id;
												this.vinInfo.fuel_type=(response.data.model_engine_fuel_id != 0)?response.data.model_engine_fuel_id : '';
												this.vinInfo.doors=response.data.model_doors_id;
												let seatsIdx=this.seat.find(x => x.id == response.data.seat_id);
												(seatsIdx) ? this.vinInfo.seats = seatsIdx['name'] : ''; 
											}
										}
									);
	}

	/**
	 * 
	 * @desc on sell car form submit saves vehicels.
	 * @access public
	 * @param formData
	 * @param form
	 */
	public sellCarData(formData: any, form: NgForm) {
		this.isloader = true;
		let index;
		let fileArray = new FormData();
		this.files.map((item, i) => {fileArray.append("MyFile[" + i + "]", item)});
		if (formData['interior_id'].length > 0) {
			formData['interior_id'].map((obj, j) => fileArray.append("vehicle_interiors[" + j + "][name]", obj.value));
		}
		fileArray.append("user_id", this.userInfo.id);
		fileArray.append("make_id", formData['make_id']);
		fileArray.append("model_id", formData['model_id']);
		fileArray.append("country_id", formData['country_id']);
		fileArray.append("city_id", formData['city_id']);
		fileArray.append("price", formData['price']);
		fileArray.append("kilometer", formData['kilometer']);
		fileArray.append("mileagetype", formData['mileagetype']);
		fileArray.append("condition_id", formData['condition_id']);
		fileArray.append("color_id", formData['color_id']);
		fileArray.append("trim_id", formData['trim_id']);
		fileArray.append("model_body_id", formData['model_body']);
		fileArray.append("model_engine_cc_id", formData['engine_size']);
		fileArray.append("model_engine_cylinders_id", formData['engine_cylinders']);
		fileArray.append("model_engine_power_ps_id", formData['horsepower']);
		fileArray.append("model_engine_fuel_id", formData['fuel_type']);
		fileArray.append("model_doors_id", formData['doors']);		
		fileArray.append("seat_id", formData['seat_id']);
		fileArray.append("vin", formData['vin']);
		fileArray.append("yearfrom", formData['yearfrom']);
		let yearIdx=this.year_from.findIndex(x=>x.name==formData['yearfrom']);
		fileArray.append("year_id", this.year_from[yearIdx]['id']);
		fileArray.append("description", formData['description']);
		fileArray.append("video", formData['video']);
		this.sellcarServices.sellCar(fileArray)
			.subscribe(
			response => {
				if (response['status'] == 'success') {
					this.router.navigate(['/']);
				}
				setTimeout(() => {
					this.isloader = false;
				}, 550);
			}
			);
	}


	/**
	 * 
	 * @desc Add vehicle documents
	 * @access public
	 * @param event
	 * @param imgTag
	 */
	public addVehicleImages(event, imgTag) {
		let j = 0;
		let thisObj = this;
		let data: any;
		let filenumber: number;
		let imageExt = ['IMAGE/JPEG', 'IMAGE/JPG', 'IMAGE/PNG', 'image/jpeg', 'image/jpg', 'image/png'];
		let k = 0;
		let imgs = document.getElementById('imageID').getElementsByClassName('multipleImageWrap');
		// let imageLength = (5 - imgs.length);
		
		// if (event.target.files.length > imageLength) {
			// this.maxImageError = "You can't upload more then 5 images";
		// }
		if (event.target.files.length > 0) {
			this.documentsArray = event.target.files;
			let files: any=[].slice.call(event.target.files);
			this.files = this.files.concat(files);
			//using of map by kshitiz//
			let myres = files.map((product, i) => {
				//return product.name;
				let ext=product.type;
				if(imageExt.indexOf(ext)!=-1){
					let j = i;
					let reader = new FileReader();
					reader.onloadend = function () {
						//fileList.push(reader.result);
						//$('#upload-list').append('<div class="upload-list-item">' + file.name + '</div>');
						let binaryString = reader.result;
						let img = document.createElement("img");
						img.src = binaryString;
						// img.alt = i.toString(); 
						//imgTag.appendChild(img);

						//now goind gor delete images
						let div = document.createElement("div");
						let idx: any = j;
						j++;
						div.setAttribute("id", idx);
						div.setAttribute("class", "clearfix pos_rel divImagesWrap");
						div.innerHTML = "<img class='thumbnail' src='" + binaryString + "'" +
							"title='hllo'/> <img src='assets/images/delete.png' class='remove_pict' style='position:absolute;right: -7px;bottom:-4px;height:24px;width:24px;'>";
						imgTag.appendChild(div);
						div.children[1].addEventListener("click", (event) => {
							let index: any = div.id;
							index = parseInt(index);
							thisObj.removeFile(i);
							j--;
							div.parentNode.removeChild(div);
						}, false);
						//now going  for delete images
					}
					reader.readAsDataURL(product);
				}
			});
			//using of map ends//
		}
	}

	/**
	 * 
	 * @desc As per the year range, get list of models
	 * @access public
	 * @param yearFrom
	 * @param yearTo
	 */
	setMakes(yearFrom:number, vinData:any){
		this.vinInfo.make_id='';
		this.vinInfo.model_id='';
		this.vinInfo.trim_id='';
		let yearFromIdx = this.year_from.find(x => x.name==yearFrom);
		let formData=new FormData();
		formData.append('yearFrom' , yearFromIdx['id']);
		this.datastorageServices.getMakeAPI(formData)
									.subscribe(
										data => {
									    	if(data.status=='success'){
												this.makes=data['data'];
												if(vinData){
													let makeObj = this.makes.find(x => x.key.toLowerCase()==vinData['Results'][0]['Make'].toLowerCase());
													this.vinInfo.make_id=(makeObj)?makeObj.id:'';
													this.getAllModels(this.vinInfo.make_id, vinData);
												}
											}
										}
									);
	}
	
	/**
	 * 
	 * @desc remove vehicle document
	 * @access public
	 * @param index
	 */
	public removeFile(index) {
		//delete this.files[index];
		this.files.splice(index, 1);
	}
	//add ends

	/**
	* @desc Restrict user to enter character values at mobile No.
	* @access public
	* @param event
    */
	public numOnly(event: any) {
		const pattern = /[0-9\+\-\ ]/;
		const inputChar = String.fromCharCode(event.charCode);
		if (!pattern.test(inputChar) && event.charCode != '0') {
			event.preventDefault();
		}
	}

	scan(){
		let self=this;
		let options = {
			types: {
				Code128: true,
				Code39: true,
				Code93: true,
				CodaBar: true,
				DataMatrix: true,
				EAN13: true,
				EAN8: true,
				ITF: true,
				QRCode: true,
				UPCA: true,
				UPCE: true,
				PDF417: true,
				Aztec: true
			},
			detectorSize: {
				width: .5,
				height: .7
			}
		}
		this.vin.nativeElement.focus();
		// cordova.plugins.barcodeScanner.scan(
		// 	function(result) {
		// 		self.vinInfo.vin=result.text;
		// 		self.getVinInfo(result.text);
		// 		self.vin.nativeElement.blur();
		// 		setTimeout(() => {
		// 			self.vin.nativeElement.focus();
		// 		}, 100);
				
		// 		console.log("We got a barcode\n" +
		// 			  "Result: " + result.text + "\n" +
		// 			  "Format: " + result.format + "\n" +
		// 			  "Cancelled: " + result.cancelled+"\n"+
		// 			  "vininfo"+ self.vinInfo.vin);
		// 	},
		// 	function(error) {
		// 		self.flashMessagesService.show(error,{classes: ['alert', 'alert-danger'], timeout: 2200});
		// 		console.log("Scanning failed: " + error);
		// 	},
		// 	{
		// 		preferFrontCamera : false, // iOS and Android
		// 		showFlipCameraButton : true, // iOS and Android
		// 		showTorchButton : true, // iOS and Android
		// 		torchOn: true, // Android, launch with the torch switched on (if available)
		// 		saveHistory: true, // Android, save scan history (default false)
		// 		prompt : "Place a barcode inside the scan area", // Android
		// 		resultDisplayDuration: 500, // Android, display scanned text for X ms. 0 suppresses it entirely, default 1500
		// 		formats : "QR_CODE,PDF_417", // default: all but PDF_417 and RSS_EXPANDED
		// 		orientation : "portrait", // Android only (portrait|landscape), default unset so it rotates with the device
		// 		disableAnimations : true, // iOS
		// 		disableSuccessBeep: false // iOS and Android
		// 	}
		//  );


		window.plugins.GMVBarcodeScanner.scan(options,function(error, result) { 

			//Handle Errors
			if(error) {
				self.flashMessagesService.show(error.message,{classes: ['alert', 'alert-danger'], timeout: 2200});
				console.log("Scanning failed: " , error);	
				return;
			}
			
			//Do something with the data.
			console.log(result);
			self.vinInfo.vin=result;
			self.getVinInfo(result);
			self.vin.nativeElement.blur();
			setTimeout(() => {
				self.vin.nativeElement.focus();
			}, 100);
		});
		
	}
}