import { Injectable } from '@angular/core'
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

@Injectable()
export class SellcarServices {
	constructor(private http: HttpClient) { }

	/**
	 * @desc Service Function to add Sell Car.
	 * @access public
	 * @param data
	 */
	public sellCar(data: FormData): Observable<any> {
		return this.http.post('vehicles/app_addSellCar', data);
	}
	
	/**
	 * @desc Service Function to get vin info from third party api.
	 * @access public
	 * @param id
	 */
	public getVinInfo(id:any): Observable<any> {
		return this.http.get('https://vpic.nhtsa.dot.gov/api/vehicles/DecodeVinValues/'+id+'?format=json');
	}
}