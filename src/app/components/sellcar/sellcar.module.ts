import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SellcarComponent } from "./sellcar.component";
import { sellcarRouter } from './sellcar.router';
import { TagInputModule } from 'ngx-chips';
import { SharedModule } from "../../shared/modules/shared.module";
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
	declarations: [
		SellcarComponent
	],
	imports: [
		sellcarRouter,
		CommonModule,
		FormsModule,
		TagInputModule,
		ReactiveFormsModule,
		SharedModule,
		TranslateModule
	]
})

export class SellcarModule { }