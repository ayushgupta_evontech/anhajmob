import { Injectable } from '@angular/core'
import { HttpClient,HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

import { Request, Response, Headers, RequestOptions } from '@angular/http';

@Injectable()
export class TestDriveBookingServices {
    
	constructor(private http: HttpClient) {}
	
	/**
	 * @desc Service function to get drive booking list.
	 * @access public
	 */
	public driveBookingList(): Observable<any> {  
		return this.http.get('users/app_driveBookingList');
	}

	/**
	 * @desc Service function to set drive booking status.
	 * @access public
	 * @param formData
	 */
	public driveBookingStatus(formData:any): Observable<any> {  
		return this.http.post('users/app_driveBookingStatus', formData);
	}
}