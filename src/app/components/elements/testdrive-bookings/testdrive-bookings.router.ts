import { Routes, RouterModule } from '@angular/router';
import { TestDriveBookingsComponent } from './testdrive-bookings.component';

const TESTDRIVE_ROUTER: Routes = [
    { 
        path: '',
        component: TestDriveBookingsComponent
    }
];

export const TestDriveBookingsRouter = RouterModule.forChild(TESTDRIVE_ROUTER);