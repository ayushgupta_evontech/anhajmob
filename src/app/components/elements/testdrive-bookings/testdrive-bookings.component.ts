import { Component ,OnInit, EventEmitter, ViewChild, ElementRef, Output, Input, SimpleChanges} from '@angular/core';
import { Router, Event as RouterEvent} from '@angular/router';
import { NgForm, FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { FlashMessagesService } from 'ngx-flash-messages';

import { TestDriveBookingServices } from './services/testdrive-bookings.service';
import { CookieService } from 'ngx-cookie-service';
import { TranslateService } from '@ngx-translate/core';

/**
 * Test Drive Bookings listing of the user.
 *  
 * @export
 * @class TestDriveBookingsComponent
 * @implements {OnInit}
 */
@Component({
	selector: 'app-testdrive-bookings',
	templateUrl: './testdrive-bookings.component.html',
	styleUrls: ['./testdrive-bookings.component.css'],
	providers:[TestDriveBookingServices]
})

export class TestDriveBookingsComponent implements OnInit {
	bookinglist:any=[];
	@ViewChild('close') closeSignIn: ElementRef;
	@Input() testdrive: boolean;
	isloader:boolean=false;
	ngOnChanges(testdrive: SimpleChanges) {
		if(testdrive.testdrive.currentValue){
			this.testDriveListing();
		}
	}
	constructor(private fb: FormBuilder,
				private testDriveService:TestDriveBookingServices,
				private _cookieService:CookieService,
				private translate:TranslateService,
				private flashMessagesService: FlashMessagesService) {

	}
	ngOnInit() {
		window.scrollTo(0, 0);
	}

	/**
	 * @desc testDriveListing Function 
	 * @access public
	 */
	public testDriveListing(){
		this.testDriveService.driveBookingList()
						.subscribe(
							response=> {
								if(response['status']=='success'){
									this.bookinglist=response['data'];
								}	
							}
						)
	}

	/**
	 * 
	 * @access public
	 * @param item
	 * @param status
	 * @desc function to update status accept/decline of requests.
	 */
	public rqstStatus(item, status){
		this.isloader=true;
		item.status=status;
		this.testDriveService.driveBookingStatus(item)
								.subscribe(
									response=>{
										this.isloader=false;
									},
								);
	}
}
