import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TestDriveBookingsComponent } from './testdrive-bookings.component';
import { TestDriveBookingsRouter } from './testdrive-bookings.router';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
	declarations: [],
	imports: [
		TestDriveBookingsRouter,
		CommonModule,
		FormsModule,
		ReactiveFormsModule,
		TranslateModule	
	],
	exports:[]	
})

export class TestDriveBookingsModule { }