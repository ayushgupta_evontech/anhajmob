import { Component, OnInit} from '@angular/core';
import { Router, Event as RouterEvent, NavigationStart, NavigationEnd} from '@angular/router';
/**
 * Footer page common for application.
 *  
 * @export
 * @class FooterComponent
 * @implements {OnInit}
 */
@Component({
	selector: 'app-footer',
	templateUrl: './footer.component.html',
	styleUrls: ['./footer.component.css']
})
export class FooterComponent implements OnInit {
	crntYear:any;
	constructor(private router: Router) {
		this.crntYear=new Date().getFullYear();
	}
	
	ngOnInit() {
	}
	
}
