import { Component, OnInit, Input, Renderer2 } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router, Event as RouterEvent, NavigationStart, NavigationEnd } from '@angular/router';
import { FlashMessagesService } from 'ngx-flash-messages';
import { ObjNgForPipe } from '../../../shared/pipes/ObjNgFor.pipe';

import { CookieService } from 'ngx-cookie-service';
import { AuthServices } from '../../auth/services/auth.service';
import { DataStorageServices } from '../../../shared/services/dataStorage.service';
import { TranslateService } from '@ngx-translate/core';

@Component({
	selector: 'app-header',
	templateUrl: './header.component.html',
	styleUrls: ['./header.component.css'],
	providers: [AuthServices, ObjNgForPipe]
})
export class HeaderComponent implements OnInit {

	userInfo: any = {};
	userMail: string;
	menu: any;
	isclick: any=[];
	isLangEn:boolean=true;
	flag:any;
	otpMail:string;
	@Input() countries:any=[];
	@Input() testdrive: any;
	i:number=0;	
	constructor(private _cookieService: CookieService,
			private authService: AuthServices,
			private router: Router,
			private renderer: Renderer2,
			private ObjNgForPipe: ObjNgForPipe,
			private translate: TranslateService,
			private flashMessagesService: FlashMessagesService,
			private datastorageServices: DataStorageServices) {
		this.menu = false;
		if(localStorage.getItem('userInfo')){
			let userInfo = localStorage.getItem('userInfo');
			let remember = localStorage.getItem('remember');
			let token = localStorage.getItem('token')
			this._cookieService.set('userInfo', JSON.parse(JSON.stringify(userInfo)));
			this._cookieService.set('remember', JSON.parse(JSON.stringify(remember)));
			this._cookieService.set('token', token);
		}else{
			this._cookieService.deleteAll();
		}
		this.setUserInfo('');
	}

	ngOnInit() { 
		this.getAllCountries(); 
		this.datastorageServices.editUser
									.subscribe(
										data=>{
											if(data==null){
												this.userInfo={};
											}
										}
								);
	}

	useLanguage(lang:string){
		this.translate.use(lang);
		this.isLangEn=(lang === 'en')?true:false;
		if(lang === 'en'){
			this.renderer.setAttribute(document.querySelector('html'), 'lang', 'en');
			this.renderer.setAttribute(document.querySelector('html'), 'dir', 'ltr');
		}else if(lang === 'ar'){
			this.renderer.setAttribute(document.querySelector('html'), 'lang', 'ar');
			this.renderer.setAttribute(document.querySelector('html'), 'dir', 'rtl');
		}

	}
	setUserInfo(data) {
		if (this._cookieService.get('userInfo')) {
			this.userInfo = JSON.parse(JSON.stringify(this._cookieService.get('userInfo')));
			this.userInfo = JSON.parse(this.userInfo);
			this.datastorageServices.editUser.next(this.userInfo);
			this.menu = true;
		}else{
			this.userInfo = {};
			this.datastorageServices.editUser.next(this.userInfo);
			this.menu = false;
			this.otpMail=data;
		}
	}

	getAllCountries() {
		this.datastorageServices.getCountryAPI()
			.subscribe(
				response => {
					this.countries = this.ObjNgForPipe.transform(response['data']);
					this.datastorageServices.countries.next(this.countries);
				}
			);
	}

	setUserEmail(data) {
		(data) ? this.userMail = data : '';
	}

	logout() {
		this.authService.logout()
			.subscribe(
			response => {
				if (response.status == 'success') {
					this._cookieService.deleteAll();
					this.userInfo = {};
					this.datastorageServices.editUser.next(this.userInfo);
					this.menu = false;
					this.router.navigate(['']);
				}
			}
			);
	}

	testdriveClick(){
		this.testdrive=++this.i;
	}
}
