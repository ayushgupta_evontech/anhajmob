import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ListingComponent } from "./listing.component";

import { ListingRouter } from './listing.router';
import { Ng5SliderModule } from 'ng5-slider';
import { NgxPaginationModule } from 'ngx-pagination';
import { SearchResolver } from './resolver/search-resolver.service';
import { SharedModule } from "../../shared/modules/shared.module";
import { TranslateModule } from '@ngx-translate/core';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';

@NgModule({
	declarations: [
		ListingComponent
	],
	imports: [
		ListingRouter,
		CommonModule,
		SharedModule,
		FormsModule,
		ReactiveFormsModule,
		Ng5SliderModule,
		NgxPaginationModule,
		InfiniteScrollModule,
		TranslateModule.forChild()
	]
})

export class ListingModule { }