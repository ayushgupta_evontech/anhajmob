import { Routes, RouterModule } from '@angular/router';
import { ListingComponent } from "./listing.component";
import { SearchResolver } from './resolver/search-resolver.service';

const LISTING_ROUTER: Routes = [
    { 
        path: '',
        component: ListingComponent,
        resolve: { message: SearchResolver },
        runGuardsAndResolvers: 'always',
    },{ 
        path: 'listing',
        component: ListingComponent,
        resolve: { message: SearchResolver },
        runGuardsAndResolvers: 'always',
    },{ 
        path: 'used-listing',
        component: ListingComponent,
        resolve: { message: SearchResolver },
        runGuardsAndResolvers: 'always',
    },{ 
        path: 'new-listing',
        component: ListingComponent,
        resolve: { message: SearchResolver },
        runGuardsAndResolvers: 'always',
    }
];

export const ListingRouter = RouterModule.forChild(LISTING_ROUTER );

