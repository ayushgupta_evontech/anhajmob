import { Injectable, Inject } from "@angular/core";
import { Resolve,ActivatedRouteSnapshot,RouterStateSnapshot } from '@angular/router';
import { Router, ActivatedRoute, Event as RouterEvent, NavigationStart, NavigationEnd } from '@angular/router';

import { HttpClient,HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Rx';
import 'rxjs/add/observable/forkJoin';

/**
 * Search Resolver to search vehicle list based on the search query.
 * 
 * @export
 * @class SearchResolver
 * @implements {Resolve}
 */
@Injectable()
export class SearchResolver implements Resolve<any> {
    constructor(private http: HttpClient, 
                private router:Router) {}
    resolve(
        route: ActivatedRouteSnapshot,
        rstate: RouterStateSnapshot
    ): Observable<any> {
        let searchObj:any = {};

        searchObj.make_id = (route.queryParams['make_id']) ? route.queryParams['make_id'] : '';
        searchObj.country_id = (route.queryParams['country_id'])? route.queryParams['country_id'] : '';
        searchObj.condition_id = (route.queryParams['condition_id'])? route.queryParams['condition_id'] : '';
        searchObj.city_id = (route.queryParams['city_id']) ? route.queryParams['city_id'] : '';
        searchObj.model_id = (route.queryParams['model_id']) ? route.queryParams['model_id'] : '';
        searchObj.yearfrom = (route.queryParams['yearfrom']) ? route.queryParams['yearfrom'] : '';
        searchObj.yearto = (route.queryParams['yearto']) ? route.queryParams['yearto'] : '';
        searchObj.price_from = (route.queryParams['price_from']) ? route.queryParams['price_from'] : '';
        searchObj.price_to = (route.queryParams['price_to']) ? route.queryParams['price_to'] : '';
        searchObj.seats = (route.queryParams['seats']) ? route.queryParams['seats'] : '';
        searchObj.kilometer = (route.queryParams['kilometer']) ? route.queryParams['kilometer'] : '';
        searchObj.interiors = (route.queryParams['interiors']) ? route.queryParams['interiors'] : '';
        searchObj.color_id = (route.queryParams['color_id']) ? route.queryParams['color_id'] :'';
        searchObj.page = (route.queryParams['page']) ? route.queryParams['page'] : '';
        searchObj.trim_id = (route.queryParams['trim_id']) ? route.queryParams['trim_id'] : '';
        let response1 = this.http.post('vehicles/app_searchResult',searchObj);
        let response2 = this.http.post('vehicles/app_countsearchResult',searchObj);
        let response3 = this.http.get('vehicles/app_getServerData',searchObj);
        return Observable.forkJoin([response1, response2, response3]);
    }

  
}