import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Options } from 'ng5-slider';
import { ObjNgForPipe } from '../../shared/pipes/ObjNgFor.pipe';
import { DataStorageServices } from '../../shared/services/dataStorage.service';
import { ListingServices } from './services/listing.service';
import { environment } from "../../../environments/environment";
import { TranslateService } from '@ngx-translate/core';
import { animateList } from '../../shared/animation/animation';


/**
 * Search result page to display listing.  
 *  
 * @export
 * @class ListingComponent
 * @implements {OnInit}
 */
@Component({
	selector: 'app-search-result',
	templateUrl: './listing.component.html',
	styleUrls: ['./listing.component.css'],
	providers: [ObjNgForPipe, ListingServices],
	animations: [animateList]
})

export class ListingComponent implements OnInit {
	isFillter: boolean;
	searchData: any;
	countSearch: any;
	isloader: boolean = false;
	show: boolean = false;
	btnName: any = 'show';
	baseUrl: any = environment.baseUrl;
	options: Options = {
		floor: 0,
		ceil: 100,
		step: 1,
		noSwitching: true
	};
	searchObj: any = {};	
	collection = [];
	currentPage: any=1;
	@Input() searchnewobj: any[];
	@Input() cartype: any
	@Input() serverData: any[];
	isFullListDisplayed:boolean= false;

	constructor(private route: ActivatedRoute,
		private ObjNgForPipe: ObjNgForPipe,
		private translate:TranslateService,
		private datastorageServices: DataStorageServices,
		private listingServices: ListingServices) {
		this.isFillter = true;
	}

	ngOnInit() {
		this.isloader = true;
		window.scroll(0, 0);
		//let searchObj: any = {};
		this.searchObj.page=1;

		// gets search query params from url
		this.route.queryParams.subscribe(queryParams => {
			this.searchObj.make_id = (queryParams['make_id']) ? queryParams['make_id'] : '';
			this.searchObj.country_id = (queryParams['country_id']) ? queryParams['country_id'] : '';
			this.searchObj.condition_id = (queryParams['condition_id']) ? queryParams['condition_id'] : '';
			this.searchObj.city_id = (queryParams['city_id']) ? queryParams['city_id'] : '';
			this.searchObj.model_id = (queryParams['model_id']) ? queryParams['model_id'] : '';
			this.searchObj.yearfrom = (queryParams['yearfrom']) ? queryParams['yearfrom'] : '';
			this.searchObj.yearto = (queryParams['yearto']) ? queryParams['yearto'] : '';
			this.searchObj.seats = (queryParams['seats']) ? queryParams['seats'] : '';
			this.searchObj.kilometer = (queryParams['kilometer']) ? queryParams['kilometer'] : '';
			this.searchObj.interiors = (queryParams['interiors']) ? queryParams['interiors'] : '';
			this.searchObj.color_id = (queryParams['color_id']) ? queryParams['color_id'] : '';
			this.cartype = queryParams['condition_id'];
			this.searchObj.page = (queryParams['page']) ? queryParams['page'] : 1;
			this.searchObj.price_from = (queryParams['price_from']) ? queryParams['price_from'] : '';
			this.searchObj.price_to = (queryParams['price_to']) ? queryParams['price_to'] : '';
			this.searchObj.trim_id = (queryParams['trim_id']) ? queryParams['trim_id'] : '';
			setTimeout(() => {
				this.isloader = false;
			}, 500);
			this.searchnewobj = this.searchObj;
		});
		
		/*this.datastorageServices.editUser.subscribe(
			data => {
						if(data && data.id){
							this.searchnewobj['country_id']=data.country_id;
							this.searchnewobj['city_id']=data.city_id;
						}
		}); */

		// get data from resolver
		this.route.data.subscribe(data => {
			if (data.message[0].status == 'success') {
				this.searchData = data.message[0]['data'];
			} else {
				this.searchData = [];
			}
			if (data.message[1].status == 'success') {
				this.countSearch = data.message[1]['count'];
			} else {
				this.countSearch = 0;
			}
			if (data.message[2].status == 'success') {
				this.serverData = data.message[2]['data'];
			} else {
				this.serverData = [];
			}
			this.show=false;
		});
	}

	/**
	 * 
	 * @access public
	 * @param event 
	 * @desc fetches all vehicles based on the search conditions.
	 */
	// public getServerData(event) {
	// 	this.searchObj.page = event;
	// 	this.currentPage = event;
	// 	this.listingServices.getdata(event, this.searchObj).subscribe(
	// 		response => {
	// 			if (response.error) {
	// 			} else {
	// 				this.searchData = response.data;
	// 				window.scroll(0,0);
	// 			}
	// 		}
	// 	);
	// 	return event;
	// }

	/**
	 * 
	 * @access public
	 * @desc set booleans condition for filter
	 */
	filtercar() {
		this.show = !this.show;
		if (this.show)
			this.btnName = "Hide";
		else
			this.btnName = "Show";
	}

	onScrollDown() {
		this.searchObj.page += this.currentPage;
		if(this.searchObj.page > 1){
			this.listingServices.getdata(this.searchObj.page, this.searchObj).subscribe(
				response => {
					if (response.error) {
					} else if(response.data.length > 0) {
						response.data.forEach(item => {
							this.searchData.push(item);
						});
					}
				}
			);
		}
	}
	 
	onScrollUp() {
		console.log('scrolled up!!');
	}
}
