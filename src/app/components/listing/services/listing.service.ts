import { Injectable } from '@angular/core'
import { HttpClient,HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

import { Request, Response, Headers, RequestOptions } from '@angular/http';

@Injectable()
export class ListingServices {
    
  constructor(private http: HttpClient) {}
	   
  	/**
	 * @desc Service Function to get search result
	 * @access public
	 * @param page
	 * @param searchObj
	 */
  	public getdata(page:Number,searchObj:any):any{
		return this.http.post('vehicles/app_searchResult',searchObj);
	}
	
	/**
	 * @desc Service Function to get search data
	 * @access public
	 * @param data
	 */
  	public searchCar(data:any): Observable<any> {  
		return this.http.post('users/app_search', data);
	}

	/**
	 * @desc Service Function to get my cars list data
	 * @access public
	 * @param userId
	 */
	public myCarLists(userId:any): Observable<any> {  
		return this.http.get('users/app_mycarslist'+userId);
	}
}