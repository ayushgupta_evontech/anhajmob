import { Injectable, Inject } from "@angular/core";
import { Resolve,ActivatedRouteSnapshot,RouterStateSnapshot } from '@angular/router';
import { HttpClient,HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Rx';
import 'rxjs/add/observable/forkJoin';

/**
 * Detail Result Resolver to get vehicle by its id.
 * 
 * @export
 * @class DetailResultResolver
 * @implements {Resolve}
 */
@Injectable()
export class DetailResultResolver implements Resolve<any> {

    constructor(private http: HttpClient) {}
    resolve(
        route: ActivatedRouteSnapshot,
        rstate: RouterStateSnapshot
    ): Observable<any> {
        let id:any=route.paramMap.get('id');
        return this.http.get('vehicles/app_getVehicleById/'+id);
    }
}