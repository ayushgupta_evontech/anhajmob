import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators, NgForm, AbstractControl, FormControl } from '@angular/forms';
import { DetailResultServices } from './services/detail-result.service';
import { environment } from "../../../environments/environment";
import { Pipe, PipeTransform } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { TranslateService } from '@ngx-translate/core';

/**
 * safe Pipe is to domSanitizer the string.
 *  
 * @export
 * @class SafePipe
 * @implements {PipeTransform}
 */
@Pipe({ name: 'safe' })
export class SafePipe implements PipeTransform {
  constructor(private sanitizer: DomSanitizer) { }
  transform(url) {
    return this.sanitizer.bypassSecurityTrustResourceUrl(url);
  }
}

/**
 * Detail result displays vehicle details.
 *  
 * @export
 * @class DetailResultComponent
 * @implements {OnInit}
 */
@Component({
	selector: 'app-detail-result',
	templateUrl: './detail-result.component.html',
	styleUrls: ['./detail-result.component.css'],
	providers: [DetailResultServices]
})

export class DetailResultComponent implements OnInit {

	slickActive: any;
	slides = [
		{ img: "./assets/images/audi a4.png" },
		{ img: "./assets/images/backort.png" },
		{ img: "./assets/images/front.png" },
		{ img: "./assets/images/top.png" },
		{ img: "./assets/images/video.png" }
	];
	slideConfig = { "slidesToShow": 4, "slidesToScroll": 4 };
	bookinglist = [];
	@Input() vehicle: any = [];
	isExcerpt: boolean = true;
	isloader: boolean = false;
	msgform: FormGroup; //Form 	
	baseUrl: any = environment.baseUrl;
	mapsurl: any;
	Objectkeys:any;
	constructor(private route: ActivatedRoute,
		private router: Router,
		private fb: FormBuilder,
		private sanitizer: DomSanitizer,
		private translate:TranslateService,
		private detailService: DetailResultServices) {
		this.Objectkeys=Object.keys;
		for (let i = 1; i <= 10; i++) {
			this.bookinglist.push(`item ${i}`);
		}
		this.msgform = this.fb.group({
			'customer_name': ['', Validators.required],
			'phone_number': ['', Validators.required],
			'customer_email': ['', Validators.compose([
				Validators.required,
				Validators.pattern('^[a-zA-Z0-9][-a-zA-Z0-9._]+@([-a-z0-9]+[.])+[a-z]{2,5}$')
			])],
			'message': ['', Validators.required],
		});
	}

	ngOnInit() {
		window.scroll(0,0);
		this.route.data.subscribe(data => {
			if (Object.keys(data.DetailResultResolver.data).length > 0) {
				this.vehicle = data.DetailResultResolver.data;
				let UserAddr:any=this.vehicle.user.address+','+this.vehicle.user.city.name+','+this.vehicle.user.city.country.name;
				this.mapsurl = 'https://maps.google.com/maps?q=' + UserAddr + '&t=&z=7&ie=UTF8&iwloc=&output=embed';
			}
		});
	}

	addSlide() { }
	removeSlide() { }
	slickInit(e) { }
	breakpoint(e) { }
	afterChange(e) {
		this.slickActive = e.currentSlide;
	}
	beforeChange(e) {}
	doExcerpt() {
		this.isExcerpt = !this.isExcerpt;
	}

	/**
	 * 
	 * @access public
	 * @param msgformData 
	 * @param form
	 * @desc Submit form dealer queries messages.
	 */
	public msgformSubmit(msgformData: any, form: NgForm) {
		this.isloader = true;
		msgformData.user_id = this.vehicle['user'].id;
		msgformData.user_name = this.vehicle['user'].name;
		msgformData.user_mail = this.vehicle['user'].email;
		msgformData.vehicle_id = this.vehicle['id'];

		this.detailService.dealerQueries(msgformData)
			.subscribe(
			response => {
				this.isloader = false;
				if (response.status == 'success') {
					form.resetForm();
				}
			}
			)
	}
	
	/**
	 * 
	 * @access public
	 * @param event
	 * @desc Restrict user to enter character values at mobile No.
	 */
	public numOnly(event: any) {
		const pattern = /[0-9\+\-\ ]/;
		const inputChar = String.fromCharCode(event.charCode);
		if (!pattern.test(inputChar) && event.charCode != '0') {
			event.preventDefault();
		}
	}
}
