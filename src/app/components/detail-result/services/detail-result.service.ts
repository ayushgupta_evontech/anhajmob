import { Injectable } from '@angular/core'
import { HttpClient,HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import { environment } from "../../../../environments/environment";
import { Request, Response, Headers, RequestOptions } from '@angular/http';

@Injectable()
export class DetailResultServices {
    constructor(private http: HttpClient) {}

	/**
	 * @desc Service Function to submit dealer Queries.
	 * @access public
	 * @param formdata
	 */
	public dealerQueries(formdata:any): Observable<any> {
		return this.http.post('vehicles/app_dealerQueries', formdata);
	}
	
	/**
	 * @desc Service function to test drive request.
	 * @access public
	 * @param formdata
	 */
	public testDriveRqst(formdata:any): Observable<any> {
		return this.http.post('vehicles/app_testDriveRqst', formdata);
	}
}