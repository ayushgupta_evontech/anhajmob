import { Routes, RouterModule } from '@angular/router';
import { DetailResultComponent } from "./detail-result.component";
import { DetailResultResolver } from './resolver/detail-result.resolver';

const DETAILRESULT_ROUTER: Routes = [
    { 
        path: '',
        component: DetailResultComponent,
        resolve: { DetailResultResolver }        
    }
];

export const DetailResultRouter = RouterModule.forChild(DETAILRESULT_ROUTER );

