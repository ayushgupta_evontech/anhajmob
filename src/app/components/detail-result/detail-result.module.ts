import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DetailResultComponent, SafePipe } from "./detail-result.component";
import { DetailResultRouter } from './detail-result.router';
import { SlickCarouselModule } from 'ngx-slick-carousel';
import { DetailResultResolver } from './resolver/detail-result.resolver';

import { TestDriveBookingComponent } from './testdrive-booking/testdrive-booking.component';
import { SharedModule } from "../../shared/modules/shared.module";
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
	declarations: [
		DetailResultComponent,
		TestDriveBookingComponent,
		SafePipe
	],
	imports: [
		DetailResultRouter,
		CommonModule,
		FormsModule,
		SharedModule,
		ReactiveFormsModule,
		SlickCarouselModule,
		TranslateModule
	],
	providers:[DetailResultResolver]
})

export class DetailResultModule { }