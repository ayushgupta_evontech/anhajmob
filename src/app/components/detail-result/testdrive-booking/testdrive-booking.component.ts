import { Component, OnInit, Input, Output, ViewChild, EventEmitter, ElementRef } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators, NgForm, AbstractControl, FormControl } from '@angular/forms';
import { DetailResultServices } from '../services/detail-result.service';
import { environment } from "../../../../environments/environment";
import { TranslateService } from '@ngx-translate/core';

/**
 * Test drive booking submit user test drive request.
 *  
 * @export
 * @class TestDriveBookingComponent
 * @implements {PipeTransform}
 */
@Component({
	selector: 'app-testdrive-booking',
	templateUrl: './testdrive-booking.component.html',
	styleUrls: ['./testdrive-booking.component.css'],
	providers:[DetailResultServices]
})


export class TestDriveBookingComponent implements OnInit {
	testDriveform: FormGroup; //Form
	@Input() vehicle:any[];
	@ViewChild('closetestDrive') closetestDrive: ElementRef;
	isloader:boolean=false;
	baseUrl:any=environment.baseUrl;
	constructor(private fb: FormBuilder,
				private translate:TranslateService,
				private detailService: DetailResultServices) {
			this.testDriveform = this.fb.group({
				'customer_name': ['', Validators.required],
				'phone_number': ['', Validators.required],
				'customer_email': ['', Validators.compose([
								Validators.required,
								Validators.pattern('^[a-zA-Z0-9][-a-zA-Z0-9._]+@([-a-z0-9]+[.])+[a-z]{2,5}$')
						])],
				'message': ['', Validators.required]
			});
	}

	ngOnInit() {}

	/**
	 * 
	 * @access public
	 * @param testDriveData
	 * @param form
	 * @desc submits test drive.
	 */
	public testDriveSubmit(testDriveData:any, form:NgForm){
		this.isloader=true;
		testDriveData.user_id=this.vehicle['user'].id;
		testDriveData.user_name=this.vehicle['user'].name;
		testDriveData.user_mail=this.vehicle['user'].email;
		testDriveData.vehicle_id=this.vehicle['id'];
		testDriveData.vehicle_name=this.vehicle['yearfrom']+' '+this.vehicle['make']['name']+' '+this.vehicle['model']['name'];

		this.detailService.testDriveRqst(testDriveData)
							.subscribe(
								response=>{
									this.isloader=false;
									if(response.status == 'success'){
										this.closetestDrive.nativeElement.click();
									}
								}
							);
	}

	/**
	 * 
	 * @access public
	 * @param form 
	 * @desc closes the test drive request form.
	 */
	public closetestDriveForm(form:NgForm){
		form.resetForm();		
	}

	/**
	 * 
	 * @access public
	 * @param form 
	 * @desc Restrict user to enter character values at mobile No.
	 */
	public numOnly(event: any) {
		const pattern = /[0-9\+\-\ ]/;
		const inputChar = String.fromCharCode(event.charCode);
		if (!pattern.test(inputChar) && event.charCode != '0') {
			event.preventDefault();
		}
	}
}
