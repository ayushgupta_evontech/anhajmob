export interface EditProfile{
    company_name?: any,
    name?: any,
    phone: any,
    email?: any,
    country_id?: any,
    city_id?: any,
    license_number?: any,
    address?: any
}