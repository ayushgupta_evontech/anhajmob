import { Routes, RouterModule } from '@angular/router';
import { EditProfileComponent } from './edit-profile.component';

const EditProfile_ROUTER: Routes = [
    { 
        path: '',
        component: EditProfileComponent
    }
];

export const EditProfileRouter = RouterModule.forChild(EditProfile_ROUTER );

