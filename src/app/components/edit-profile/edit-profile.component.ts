import { Component, OnInit, Input, Output, EventEmitter, ViewChild, ElementRef } from '@angular/core';
import { FormGroup, FormBuilder, Validators, AbstractControl, FormControl } from '@angular/forms';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { ObjNgForPipe } from '../../shared/pipes/ObjNgFor.pipe';

import { DataStorageServices } from '../../shared/services/dataStorage.service';
import { AuthServices } from '../auth/services/auth.service';
import { CookieService } from 'ngx-cookie-service';

import { Country } from '../../shared/models/country';
import { City } from '../../shared/models/city';
import { EditProfile } from './model/edit-profile';
import { TranslateService } from '@ngx-translate/core';

/**
 * Edit Profile displays and can edit user profile.
 *  
 * @export
 * @class EditProfileComponent
 * @implements {OnInit}
 */
@Component({
	selector: 'app-edit-profile',
	templateUrl: './edit-profile.component.html',
	styleUrls: ['./edit-profile.component.css'],
	providers: [AuthServices,ObjNgForPipe]
})


export class EditProfileComponent implements OnInit {
	cities: City[];
	editProfile:any=[];
	isIndividual: boolean;
	isloader: boolean;
	editUserForm: FormGroup; //Form 
	@Input() countries:Country[];
	@ViewChild('closeEditUserForm') closeEditUserForm: ElementRef;
	// @ViewChild('otpModal') otpModal: ElementRef; /* add for otp */
	@Output() isLogin : EventEmitter <boolean> = new EventEmitter<boolean>();	
	constructor(private router:Router,
		private fb: FormBuilder,
		private datastorageServices: DataStorageServices,
		private authServices: AuthServices,
		private ObjNgForPipe: ObjNgForPipe,
		private translate:TranslateService,
		private _cookieService: CookieService) {
		this.editUserForm = this.fb.group({
			'company_name': [this.editProfile.company_name],
			'name': [''],
			'phone': [''],
			'email': [''],
			'country_id': [''],
			'city_id': [''],
			'license_number': [''],
			'address': [''],
		}, { validator: this.ValidateOption.bind(this) });		
		this.isloader = false;
	}

	ngOnInit() {
		this.datastorageServices.editUser.subscribe(
			data => {
						if(data && data.id){
							this.editProfile=data;
							this.isIndividual=(this.editProfile.usertype==1)?true:false;
							this.getAllCities(this.editProfile.country_id);
							this.ValidateOption(this.editUserForm);
						}						
					});
	}
	
	ngOnDestroy() {
		this.datastorageServices.editUser.unsubscribe();
	}

	/**
	 * 
	 * @access public
	 * @param input 
	 * @desc Applies dynamic validations based on the Dealer or Individual. 
	 */
	public ValidateOption(input: AbstractControl) {
		input.get('name').setValidators(Validators.required);
		input.get('name').updateValueAndValidity({ emitEvent: true, onlySelf: true });
		
		input.get('email').setValidators(Validators.compose([
			Validators.required,
			Validators.pattern('^[a-zA-Z0-9][-a-zA-Z0-9._]+@([-a-z0-9]+[.])+[a-z]{2,5}$')
		]));
		input.get('email').updateValueAndValidity({ emitEvent: true, onlySelf: true });
		
		input.get('phone').setValidators(Validators.compose([Validators.minLength(6), Validators.required]));
		input.get('phone').updateValueAndValidity({ emitEvent: true, onlySelf: true });
		
		input.get('country_id').setValidators(Validators.required);
		input.get('country_id').updateValueAndValidity({ emitEvent: true, onlySelf: true });

		input.get('city_id').setValidators(Validators.required);
		input.get('city_id').updateValueAndValidity({ emitEvent: true, onlySelf: true });

		if (this.isIndividual == true) {
			input.get('company_name').setValidators(null);
			input.get('company_name').updateValueAndValidity({ emitEvent: true, onlySelf: true });

			input.get('license_number').setValidators(null);
			input.get('license_number').updateValueAndValidity({ emitEvent: true, onlySelf: true });

			input.get('address').setValidators(null);
			input.get('address').updateValueAndValidity({ emitEvent: true, onlySelf: true });
		} else if (this.isIndividual == false) {
			input.get('company_name').setValidators(Validators.required);
			input.get('company_name').updateValueAndValidity({ emitEvent: true, onlySelf: true });

			input.get('license_number').setValidators(Validators.required);
			input.get('license_number').updateValueAndValidity({ emitEvent: true, onlySelf: true });

			input.get('address').setValidators(Validators.required);
			input.get('address').updateValueAndValidity({ emitEvent: true, onlySelf: true });
		}
	}

	/**
	 * @desc function to get all city list by country id.
	 * @access public
	 * @param countryid
	 */
	public getAllCities(countryid) {
		if(countryid){			
			this.datastorageServices.getCityAPI(countryid)
				.subscribe(
					response => {
						this.cities = this.ObjNgForPipe.transform(response['data']);
					}
				)
		}
	}

	/**
	 * @desc function to submit the user profile.
	 * @access public
	 * @param formData 
	 * @param form
	 */
	public editUserSubmit(formData: any, form: NgForm) {
		this.isloader=true;
		formData['usertype'] = (this.isIndividual) ? '1' : '2';
		this.authServices.editProfile(formData)
			.subscribe(
			response => {
				if(response['status']=='success'){
					let cookieLifeSpan:any;
					if(this._cookieService.get('remember')=='true'){
						cookieLifeSpan=new Date(new Date().getFullYear() + 1, new Date().getMonth(), new Date().getDate());
					}
					this._cookieService.delete('userInfo');
					/* remove for otp starts */
					this._cookieService.set('userInfo', JSON.stringify(response['data']), cookieLifeSpan);
					this.datastorageServices.editUser.next(response['data']);
					this.closeEditUserForm.nativeElement.click();
					/* remove for otp end */

					/* add for otp starts */
					/*if(response['data']['is_block'] && (response['data']['otp_status']==0)){
						this._cookieService.set('userInfo', JSON.stringify(response['data']), cookieLifeSpan);
						this.datastorageServices.editUser.next(response['data']);
					}else{
						this._cookieService.deleteAll();
						this.isLogin.emit(response['data']['email']);
						this.otpModal.nativeElement.click();
					}*/
					/* add for otp ends */
					this.closeEditUserForm.nativeElement.click();					
				}
				setTimeout(() => {
					this.isloader = false;
				}, 550);

			}
			)
	}

	/**
	 * 
	 * @desc Login Function
	 * @access public
	 * @param editUserModal
	 */
	public closeForm(editUserModal: NgForm) {
		if(this._cookieService.get('userInfo')){
			this.editProfile=JSON.parse(JSON.stringify(this._cookieService.get('userInfo')));
			this.editProfile=JSON.parse(this.editProfile);
			// editUserModal.resetForm();
		}
	}

	/**
	 * 
	 * @desc Restrict user to enter character values at mobile No. Only allows numeric value.
	 * @param event
	 * @access public
    */
	public numOnly(event: any) {
		const pattern = /[0-9\+\-\ ]/;
		const inputChar = String.fromCharCode(event.charCode);
		if (!pattern.test(inputChar) && event.charCode != '0') {
			event.preventDefault();
		}
	}
}