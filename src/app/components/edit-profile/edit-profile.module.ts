import { NgModule } from '@angular/core';
import { EditProfileComponent } from './edit-profile.component';

import { EditProfileRouter } from './edit-profile.router';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
	declarations: [],
	imports: [EditProfileRouter,TranslateModule]
})

export class EditProfileModule { }