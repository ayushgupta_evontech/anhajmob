import { Component, OnInit, Input, Output, EventEmitter, SimpleChanges } from '@angular/core';
import { NgForm, FormGroup, FormBuilder, Validators, FormControl, AbstractControl } from '@angular/forms';
import { FiltersearchServices } from './services/filtersearch.service';
import { DataStorageServices } from '../../shared/services/dataStorage.service';
import { ObjNgForPipe } from '../../shared/pipes/ObjNgFor.pipe';
import { Router, ActivatedRoute } from '@angular/router';
import { Options } from 'ng5-slider';
import { CookieService } from 'ngx-cookie-service';
import { TranslateService } from '@ngx-translate/core';

/**
 * Filter search result to modify listing.  
 *  
 * @export
 * @class FiltersearchComponent
 * @implements {OnInit}
 */
@Component({
	selector: 'app-filtersearch',
	templateUrl: './filtersearch.component.html',
	styleUrls: ['./filtersearch.component.css'],
	providers: [ObjNgForPipe, FiltersearchServices]
})


export class FiltersearchComponent implements OnInit {
	makes: any;
	conditions: any;
	cities: any;
	colors: any;
	models: any;
	interiors: any;
	seats: any;
	kilometres: any;
	countries: any;
	isFillter: boolean;
	public show: boolean = true;
	public btnName: any = 'show';
	year_from:any=[];
	year_to:any;
	value: number;
	highValue: number;
	options: Options = {
	  floor: 0,
	  ceil: 100000
	};
	@Input() serverData:any[];
	addSearchForm: FormGroup; //Form 
	@Input() cartype:any;
	@Input() searchnewobj: any;
	isLoader:boolean;
	userInfo:any=[];
	trim:any=[];
	constructor(
		private fb: FormBuilder,
		private filtersearchServices: FiltersearchServices,
		private datastorageServices: DataStorageServices,
		private ObjNgForPipe: ObjNgForPipe,
		private router: Router,
		private translate: TranslateService,		
		private _cookieService:CookieService
	) {
		this.isFillter = true;
		this.addSearchForm = this.fb.group({
			'yearfrom': [''],
			'make_id': [''],
			'model_id': [''],
			'trim_id':[''],
			'country_id': [''],
			'condition_id': [''],
			'city_id': [''],
			'yearto': [''],
			'kilometer': [''],
			'color_id': [''],
			'interiors': [''],
			'seats': [''],
			'price_range':['']
		}, { validator: this.getYearTo });
		
		let date:Date=new Date();
		//this.year_from=date.getFullYear();
		this.isLoader=false;
	}

	ngOnInit() {
		this.value=this.searchnewobj.price_from;
		this.highValue=(this.searchnewobj.price_to) ? this.searchnewobj.price_to : parseInt(this.serverData['maxPrice']);
		if(this.serverData){
			this.year_from=this.serverData['years'];
			this.countries = this.ObjNgForPipe.transform(this.serverData['Country']);
			this.makes = (typeof this.year_from!=='undefined' && this.year_from.length > 0) ? this.ObjNgForPipe.transform(this.serverData['Make']) : [];
			this.interiors = this.serverData['Interior'];
			this.conditions = this.serverData['condition'];
			this.seats = this.serverData['Vehicleseat'];
			this.kilometres = this.serverData['Kilometer'];
			this.colors = this.serverData['color'];
			this.options['ceil']= parseInt(this.serverData['maxPrice']);
		}
		if(this.searchnewobj.yearto){
			this.setYeatTo(this.searchnewobj.yearfrom);
		}
		let model_id=this.searchnewobj.model_id;
		if(this.searchnewobj['make_id']){
			this.getAllModels(this.searchnewobj['make_id']);
		}
		if(model_id){
			this.getAllTrims(this.searchnewobj.make_id, model_id);
		}
		this.getAllCities(this.searchnewobj['country_id']);
	}

	/**
	 * 
	 * @desc As per the model id, get list of all trims.
	 * @access public
	 * @param model_id
	 */
	getAllTrims(make_id:any,model_id:any){
		// this.searchnewobj.trim_id='';
		let formData=new FormData();
		if(this.searchnewobj.yearto && this.searchnewobj.yearfrom){			
			let yearFromIdx = this.year_from.find(x => x.name==this.searchnewobj.yearfrom);
			let yearToIdx = this.year_from.find(x => x.name==this.searchnewobj.yearto);
			formData.append('yearFrom', yearFromIdx['id']);
			formData.append('yearTo', yearToIdx['id']);
		}
		formData.append('make_id', make_id);
		formData.append('model_id', model_id);
		this.datastorageServices.getTrimsAPI(formData)
									.subscribe(
										data => {
											if(data.status=='success'){
												this.trim=data['data'];
											}else{
												this.trim=[];
											}
										}
									);
	}

	/**
	 * 
	 * @access public
	 * @param makeid 
	 * @desc fetches all Models based on the make with its make id passed
	 */
	public getAllModels(make_id) {
		let model_id=this.searchnewobj.model_id;
		this.searchnewobj.model_id=''
		// this.searchnewobj.trim_id='';
		let formData=new FormData();
		if(this.searchnewobj.yearto && this.searchnewobj.yearfrom){
			let yearFromIdx = this.year_from.find(x => x.name==this.searchnewobj.yearfrom);
			let yearToIdx = this.year_from.find(x => x.name==this.searchnewobj.yearto);
			formData.append('yearFrom' , yearFromIdx['id']);
			formData.append('yearTo' , yearToIdx['id']);
		}
		formData.append('make_id' , make_id);
		this.datastorageServices.getModelAPI(formData)
			.subscribe(
				data => {
					if(data.status=='success'){
						this.models=data['data'];
						(model_id)?this.searchnewobj.model_id=model_id:'';
					}else{
						this.models=[];
					}
				}
			);
	}


	/**
	 * @desc Match Password Function to reset password. 
	 * @access public
	 * @param AC
	 */
	public getYearTo(AC: AbstractControl) {
		const yearfrom = AC.get('yearfrom').value;
		const yearto = AC.get('yearto').value;
		if (yearfrom && yearto=='') {
			AC.get('yearto').setErrors({ getYearTo: true });
		} else {
			AC.get('yearto').setErrors(null);
		}
	}

	/**
	 * @desc function to get all city list by country id.
	 * @access public
	 * @param countryid 
	 * @param reset
	 */
	public getAllCities(countryid:any, reset:boolean=false) {
		// countryid=(typeof countryid =='number') ? countryid : countryid.target.value;
		if(countryid){
			this.datastorageServices.getCityAPI(countryid)
				.subscribe(
					response => {
						this.cities = this.ObjNgForPipe.transform(response['data']);
						if(reset){
							this.searchnewobj.city_id='';							
						}
					}
				)
		}else{
			this.cities=[];
			this.searchnewobj.city_id='';
		}
	}

		/**
	 * 
	 * @desc As per the year range, get list of models
	 * @access public
	 * @param yearFrom
	 * @param yearTo
	 */
	public setMakes(yearFrom:number, yearTo:number){
		this.searchnewobj.make_id='';
		this.searchnewobj.model_id='';
		// this.searchnewobj.trim_id='';
		let yearFromIdx = this.year_from.find(x => x.name==yearFrom);
		let yearToIdx = this.year_from.find(x => x.name==yearTo);
		let formData=new FormData();
		formData.append('yearFrom' , yearFromIdx['id']);
		formData.append('yearTo' , yearToIdx['id']);
		this.datastorageServices.getMakeAPI(formData)
									.subscribe(
										data => {
											if(data.status=='success'){
												this.makes=data['data'];
											}
										}
									);
	}


	
	/**
	 * 
	 * @access public
	 * @desc set booleans condition for filter
	 */
	public filtercar() {
		this.show = !this.show;
		if (this.show){
			this.btnName = "Hide";
		}else{
			this.btnName = "Show";
		}
	}

	/**
	 * 
	 * @desc As per the search and search query, redirects to show that listing page.
	 * @access public
	 * @param formData
	 * @param form
	 */
	public searchCarData(formData: any, form: NgForm) {
		this.isLoader=true;
		if(formData['price_range']){
			formData['price_from']= formData['price_range'][0];
			formData['price_to']= formData['price_range'][1];
		}		
		window.scroll(0, 0);
		if (formData['condition_id'] == "1") {
			setTimeout(()=> {
				this.isLoader=false;	
			}, 500);
			this.router.navigate(['listing/new-listing'], { queryParams: formData });
		}
		else if (formData['condition_id'] == "2") {
			setTimeout(()=> {
				this.isLoader=false;	
			}, 500);
			this.router.navigate(['listing/used-listing'], { queryParams: formData });
		}else {
			setTimeout(()=> {
				this.isLoader=false;	
			}, 500);
			this.router.navigate(['listing'], { queryParams: formData });
		}
	}

	/**
	 * 
	 * @desc function to filter search and search result listing.
	 * @access public
	 * @param addSearchForm 
	 */
	public resetFilters(addSearchForm:NgForm){
		let formData:any=[];
		this.value= 0;
		this.highValue= this.serverData['maxPrice'];
		this.searchnewobj.yearto='';
		this.searchnewobj.yearfrom='';
		this.searchnewobj.city_id='';
		this.searchnewobj.country_id='';
		Object.keys(this.addSearchForm.value).map((field)=>formData[field]='');
		this.searchCarData(formData, addSearchForm);
	}

	/**
	 * 
	 * @desc function to set year upto list filter based on yearfrom 
	 * @access public
	 * @param yearfrom 
	 */
	public setYeatTo(yearfrom: any) {
		this.addSearchForm.controls['yearto'].setValue('');
		this.year_to = yearfrom;
	}
}
