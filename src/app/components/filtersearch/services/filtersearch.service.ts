import { Injectable } from '@angular/core'
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

@Injectable()
export class FiltersearchServices {
	constructor(private http: HttpClient) { }

	/**
	 * @desc Service function to add sell car
	 * @access public
	 * @param data
	 */
	public sellCar(data: any): Observable<any> {
		return this.http.post('users/app_addSellCar', data);
	}
}