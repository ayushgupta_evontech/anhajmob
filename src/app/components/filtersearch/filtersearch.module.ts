import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FiltersearchComponent } from "./filtersearch.component";
import { FiltersearchRouter } from './filtersearch.router';
import { SharedModule } from "../../shared/modules/shared.module";
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
	declarations: [],
	imports: [
		FiltersearchRouter,
		CommonModule,
		FormsModule,
		SharedModule,
		ReactiveFormsModule,
		TranslateModule	
	]
})

export class FiltersearchModule { }


