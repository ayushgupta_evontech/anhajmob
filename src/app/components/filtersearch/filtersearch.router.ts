import { Routes, RouterModule } from '@angular/router';
import { FiltersearchComponent } from "./filtersearch.component";

const FILTERSEARCH_ROUTER: Routes = [
    { 
        path: '',
        component: FiltersearchComponent
    }
];

export const FiltersearchRouter = RouterModule.forChild(FILTERSEARCH_ROUTER );

