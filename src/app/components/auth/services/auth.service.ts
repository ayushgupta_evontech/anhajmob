import { Injectable } from '@angular/core'
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import { environment } from "../../../../environments/environment";
import { Request, Response, Headers, RequestOptions } from '@angular/http';

@Injectable()
export class AuthServices {
	constructor(private http: HttpClient) { }

    /**
	 * @desc Service Function to login
	 * @access public
	 * @param credentials
	 */
	public login(credentials: any): Observable<any> {
		return this.http.post('users/app_login', credentials);
	}

	/**
	 * @desc Service Function to logout
	 * @access public
	 */
	public logout(): Observable<any> {
		return this.http.get('users/app_logout');
	}

	/**
	 * @desc Service Function to register user
	 * @access public
	 * @param data
	 */
	public signUpUser(data: any): Observable<any> {
		return this.http.post('users/app_signUp', data);
	}

	/**
	 * @desc user edit profile
	 * @access public
	 * @param data 
 	*/
	public editProfile(data: any): Observable<any> {
		return this.http.post('users/app_editProfile', data);
	}

	/**
	 * @desc Check user Activation key
	 * @access public
	 * @param code 
 	*/
	public checkUserActivationKey(code: any): Observable<any> {
		let body = { activationKey: code };
		return this.http.post('users/app_checkUserActivationKey', body)
	}
}