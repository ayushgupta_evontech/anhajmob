import { Component ,OnInit, ViewChild, ElementRef, Input} from '@angular/core';
import { Router, Event as RouterEvent} from '@angular/router';
import { NgForm, FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { Observable } from 'rxjs/Rx';
import { FlashMessagesService } from 'ngx-flash-messages';

import { OtpVerificationServices } from './services/otp-verification.service';
import { CookieService } from 'ngx-cookie-service';
import { ForgotPasswordServices } from '../forgot-password/services/forgot-password.service';
import { TranslateService } from '@ngx-translate/core';

@Component({
	selector: 'app-otp-verification',
	templateUrl: './otp-verification.component.html',
	providers:[OtpVerificationServices, ForgotPasswordServices]
})
export class OtpVerificationComponent implements OnInit {
	
	otpForm: FormGroup;
	isloader:boolean=false;
	countDown;
    count=60;
	isdisabled:boolean=false;
	@Input() userMail:string;
	@ViewChild('closeOtp') closeOtp: ElementRef;
	@ViewChild('chngePwd') chngePwd: ElementRef;	
	constructor(private fb: FormBuilder,
				public otpVerificationService:OtpVerificationServices,
				private translate:TranslateService,
				public _cookieService:CookieService,
				public forgotPwdService:ForgotPasswordServices,
				private flashMessagesService: FlashMessagesService) {
		this.otpForm = this.fb.group({
			'otp': ['', Validators.compose([Validators.required])]
		});
		this.isloader=false;
	}
	ngOnInit() {}

	/**
	 * forgot Function to reset password. 
	 * @access public
	 * @param : Form data
	 * @param : Form reference
	 */
	otpSubmit(formData:any, form:NgForm){
		this.isloader=true;
		let formdata: FormData = new FormData();
		formdata.append('otp', formData.otp);
		formdata.append('email', this.userMail);
		this.otpVerificationService.otpVerification(formdata)
										.subscribe(
											response=> {
												if(response['status']=='success'){
													if(this.userMail){
														this.chngePwd.nativeElement.click();
													}
													this.closeOtp.nativeElement.click();
												}
												setTimeout(()=> {
													this.isloader=false;
												}, 500);
											}
										);
	}

	/**
	 * Login Function 
	 * @access public
	 * @param : otp Modal form reference
	 */
	closeForm(forgotModal:NgForm){
		forgotModal.resetForm();
	}

	/**
	* resend otp Function.
	*
	* @desc : function counter to that counts for the 59 secs to resend code.
	* @access public
    */
    resend(){
		this.isdisabled = true;
		this.countDown = Observable.timer(0, 1000)
									.take(this.count)
									.map(() =>
											((--this.count != 0) ? this.count : this.resendService())
									);
	}
	
	/**
	* resend otp Function.
	*
	* @desc : function counter to that counts for the 59 secs to resend code.
	* @access public
    */
	resendService(){
		let formdata:FormData=new FormData();
		formdata.append('email', this.userMail);			
		this.forgotPwdService.forgotPassword(formdata)
								.subscribe(
									response=>{
										if(response['status']=='success'){
											this.count = 59;
											this.isdisabled = false;
										}											
									}
								);
	}

	/**
    * @desc : Restrict user to enter character values at mobile No.
    */
	numOnly(event: any) {
		const pattern = /[0-9\+\-\ ]/;
		const inputChar = String.fromCharCode(event.charCode);
		if (!pattern.test(inputChar) && event.charCode != '0') {
			event.preventDefault();
		}
	}
}
