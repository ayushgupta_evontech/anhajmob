import { Routes, RouterModule } from '@angular/router';
import { OtpVerificationComponent } from './otp-verification.component';

const OtpVerification_ROUTER: Routes = [
    { 
        path: '',
        component: OtpVerificationComponent
    }
];

export const OtpVerificationRouter = RouterModule.forChild(OtpVerification_ROUTER);