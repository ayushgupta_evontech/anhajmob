import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { OtpVerificationComponent } from './otp-verification.component';
import { OtpVerificationRouter } from './otp-verification.router';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
	declarations: [],
	imports: [
		OtpVerificationRouter,
		CommonModule,
		FormsModule,
		ReactiveFormsModule,
		TranslateModule	
	],
	exports:[]	
})

export class OtpVerificationModule { }