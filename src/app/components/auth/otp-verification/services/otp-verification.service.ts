import { Injectable } from '@angular/core'
import { HttpClient,HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import { environment } from "../../../../../environments/environment";
import { Request, Response, Headers, RequestOptions } from '@angular/http';

@Injectable()
export class OtpVerificationServices {
    constructor(private http: HttpClient) {}

	/**
	 * @desc Service Function to reset password via mail
	 */
	public otpVerification(formdata): Observable<any> {
		return this.http.post('users/app_verifyOTP', formdata);
	}

	/**
	 * @desc Service Function to resend otp via sms
	 */
	public resendOTP(formdata): Observable<any> {
		return this.http.post('users/app_resendOTP', formdata);
	}
}