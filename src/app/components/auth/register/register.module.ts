import { NgModule } from '@angular/core';
import { RegisterComponent } from './register.component';

import { registerRouter } from './register.router';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
	declarations: [],
	imports: [registerRouter,
				TranslateModule]
})

export class RegisterModule { }