import { Component, OnInit, Input, Output, EventEmitter, ViewChild, ElementRef } from '@angular/core';
import { FormGroup, FormBuilder, Validators, NgForm, AbstractControl, FormControl } from '@angular/forms';
import { ObjNgForPipe } from '../../../shared/pipes/ObjNgFor.pipe';
import { DataStorageServices } from '../../../shared/services/dataStorage.service';
import { AuthServices } from '../services/auth.service';

import { Country } from '../../../shared/models/country';
import { City } from '../../../shared/models/city';
import { TranslateService } from '@ngx-translate/core';

@Component({
	selector: 'app-register',
	templateUrl: './register.component.html',
	styleUrls: ['./register.component.css'],
	providers: [AuthServices, ObjNgForPipe]
})


export class RegisterComponent implements OnInit {
	cities: any[];
	@Input() countries:any[];
	isIndividual: boolean;
	isloader: boolean;
	addUserForm: FormGroup; //Form 
	@ViewChild('closeAddForm') closeAddForm: ElementRef;
	constructor(private fb: FormBuilder,
		private translate :TranslateService,
		private datastorageServices: DataStorageServices,
		private authServices: AuthServices,
		private ObjNgForPipe: ObjNgForPipe) {
		this.addUserForm = this.fb.group({
			'company_name': [''],
			'name': [''],
			'phone': [''],
			'email': [''],
			'country_id': [null],
			'city_id': [null],
			'license_number': [''],
			'password': [''],
			'confirmpassword': [''],
			'address': [''],
		}, { validator: this.ValidateOption.bind(this) });

		this.isIndividual = true;
		this.isloader = false;
	}

	ngOnInit() {}

	userType() {
		this.isIndividual = !this.isIndividual;
		this.addUserForm.reset();
		this.ValidateOption(this.addUserForm);
	}
	ValidateOption(input: AbstractControl) {
		input.get('name').setValidators(Validators.required);
		input.get('name').updateValueAndValidity({ emitEvent: true, onlySelf: true });
		
		input.get('email').setValidators(Validators.compose([
			Validators.required,
			Validators.pattern('^[a-zA-Z0-9][-a-zA-Z0-9._]+@([-a-z0-9]+[.])+[a-z]{2,5}$')
		]));
		input.get('email').updateValueAndValidity({ emitEvent: true, onlySelf: true });
		
		input.get('phone').setValidators(Validators.compose([Validators.minLength(10), Validators.required]));
		input.get('phone').updateValueAndValidity({ emitEvent: true, onlySelf: true });
		
		input.get('country_id').setValidators(Validators.required);
		input.get('country_id').updateValueAndValidity({ emitEvent: true, onlySelf: true });

		input.get('city_id').setValidators(Validators.required);
		input.get('city_id').updateValueAndValidity({ emitEvent: true, onlySelf: true });
		
		input.get('password').setValidators(Validators.compose([Validators.pattern('^.*(?=.{5,15})[a-zA-Z0-9]*$'), Validators.required]));
		input.get('password').updateValueAndValidity({ emitEvent: true, onlySelf: true });
		
		const password = input.get('password').value; // to get value in input tag
		const confirmPassword = input.get('confirmpassword').value;
		if (password != confirmPassword) {
			input.get('confirmpassword').setErrors({ MatchPassword: true });
		} else {
			input.get('confirmpassword').setErrors(null);
			input.get('confirmpassword').setValidators(Validators.compose([Validators.pattern('^.*(?=.{5,15})[a-zA-Z0-9]*$'), Validators.required]));
			input.get('confirmpassword').updateValueAndValidity({ emitEvent: true, onlySelf: true });
		}

		if (this.isIndividual == true) {
			input.get('company_name').setValidators(null);
			input.get('company_name').updateValueAndValidity({ emitEvent: true, onlySelf: true });

			input.get('license_number').setValidators(null);
			input.get('license_number').updateValueAndValidity({ emitEvent: true, onlySelf: true });

			input.get('address').setValidators(null);
			input.get('address').updateValueAndValidity({ emitEvent: true, onlySelf: true });
		} else if (this.isIndividual == false) {
			input.get('company_name').setValidators(Validators.required);
			input.get('company_name').updateValueAndValidity({ emitEvent: true, onlySelf: true });

			input.get('license_number').setValidators(Validators.required);
			input.get('license_number').updateValueAndValidity({ emitEvent: true, onlySelf: true });

			input.get('address').setValidators(Validators.required);
			input.get('address').updateValueAndValidity({ emitEvent: true, onlySelf: true });
		}
	}

	getAllCities(countryid) {
		if(countryid){
			this.datastorageServices.getCityAPI(countryid)
				.subscribe(
					response => {
						this.cities = this.ObjNgForPipe.transform(response['data']);
					}
				)
		}
	}

	signUpData(formData: any, form: NgForm) {
		this.isloader=true;
		formData['usertype']=(this.isIndividual) ? '1' : '2';
		this.authServices.signUpUser(formData)
			.subscribe(
			response => {
				if(response['status']=='success'){
					this.closeAddForm.nativeElement.click();
				}
				setTimeout(() => {
					this.isloader = false;
				}, 550);
			}
			)
	}

	/**
	 * Login Function 
	 * @access public
	 * @param : sign up Modal form reference
	 */
	closeForm(forgotModal: NgForm) {
		forgotModal.resetForm();
	}

	/**
    * @desc : Restrict user to enter character values at mobile No.
    */
	numOnly(event: any) {
		const pattern = /[0-9\+\-\ ]/;
		const inputChar = String.fromCharCode(event.charCode);
		if (!pattern.test(inputChar) && event.charCode != '0') {
			event.preventDefault();
		}
	}
}