import { Component ,OnInit, Output, ViewChild, ElementRef, EventEmitter} from '@angular/core';
import { Router, Event as RouterEvent} from '@angular/router';
import { NgForm, FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { FlashMessagesService } from 'ngx-flash-messages';

import { ForgotPasswordServices } from './services/forgot-password.service';
import { CookieService } from 'ngx-cookie-service';
import { TranslateService } from '@ngx-translate/core';

@Component({
	selector: 'app-forgot-password',
	templateUrl: './forgot-password.component.html',
	providers:[ForgotPasswordServices]
})
export class ForgotPasswordComponent implements OnInit {
	
	forgotForm: FormGroup;
	isloader:boolean=false;
	@Output() isForgotPwd : EventEmitter <boolean> = new EventEmitter<boolean>();	
	@ViewChild('closeForgot') closeForgot: ElementRef;	
	@ViewChild('otpModal') otpModal: ElementRef;
	constructor(private fb: FormBuilder,
				private translate:TranslateService,
				public _cookieService:CookieService,
				public forgotPwdService:ForgotPasswordServices,
				private flashMessagesService: FlashMessagesService) {
		this.forgotForm = this.fb.group({
			'email': ['', Validators.compose([Validators.required,
							Validators.pattern('^[a-zA-Z0-9][-a-zA-Z0-9._]+@([-a-z0-9]+[.])+[a-z]{2,5}$')])]
		});
		this.isloader=false;

	}
	ngOnInit() {}

	/**
	 * forgot Function to reset password. 
	 * @access public
	 * @param : Form data
	 * @param : Form reference
	 */
	forgot(formData:any, form:NgForm){
		this.isloader=true;
		let formdata: FormData = new FormData();
		formdata.append('email', formData.email);
		this.forgotPwdService.forgotPassword(formdata)
						.subscribe(
							response=> {
								this.isloader=false;
								if(response['status']=='success'){
									this.closeForgot.nativeElement.click();
									this.otpModal.nativeElement.click();
									this.isForgotPwd.emit(formData.email);
								}
							}
						)
	}

	/**
	 * Login Function 
	 * @access public
	 * @param : forgot Modal form reference
	 */
	closeForm(forgotModal:NgForm){
		forgotModal.resetForm();
	}
}
