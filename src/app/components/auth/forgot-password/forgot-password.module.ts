import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ForgotPasswordComponent } from "./forgot-password.component";
import { ForgotPasswordRouter } from './forgot-password.router';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
	declarations: [],
	imports: [
		ForgotPasswordRouter,
		CommonModule,
		FormsModule,
		ReactiveFormsModule,
		TranslateModule	
	],
	exports:[]	
})

export class ForgotPasswordModule { }