import { Routes, RouterModule } from '@angular/router';
import { ForgotPasswordComponent } from "./forgot-password.component";

const ForgotPassword_ROUTER: Routes = [
    { 
        path: '',
        component: ForgotPasswordComponent
    }
];

export const ForgotPasswordRouter = RouterModule.forChild(ForgotPassword_ROUTER);