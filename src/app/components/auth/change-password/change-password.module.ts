import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ChangePasswordComponent } from "./change-password.component";
import { ChangePasswordRouter } from './change-password.router';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
	declarations: [],
	imports: [
		ChangePasswordRouter,
		CommonModule,
		FormsModule,
		ReactiveFormsModule,
		TranslateModule
	],
	exports:[]	
})

export class changePasswordModule { }