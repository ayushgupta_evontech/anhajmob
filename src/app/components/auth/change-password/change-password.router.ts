import { Routes, RouterModule } from '@angular/router';
import { ChangePasswordComponent } from "./change-password.component";

const ChangePassword_ROUTER: Routes = [
    { 
        path: '',
        component: ChangePasswordComponent
    }
];

export const ChangePasswordRouter = RouterModule.forChild(ChangePassword_ROUTER);