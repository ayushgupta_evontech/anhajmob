import { Component ,OnInit, ViewChild, ElementRef} from '@angular/core';
import { Router, Event as RouterEvent} from '@angular/router';
import { NgForm, FormGroup, FormBuilder, Validators, FormControl, AbstractControl } from '@angular/forms';
import { FlashMessagesService } from 'ngx-flash-messages';

import { ChangePasswordServices } from './services/change-password.service';
import { CookieService } from 'ngx-cookie-service';
import { TranslateService } from '@ngx-translate/core';

@Component({
	selector: 'app-change-password',
	templateUrl: './change-password.component.html',
	providers:[ChangePasswordServices]
})
export class ChangePasswordComponent implements OnInit {
	
	changePwdForm: FormGroup;
	isloader:boolean;
	@ViewChild('closeChangePwd') closeChangePwd: ElementRef;	
	constructor(private fb: FormBuilder,
				private translate:TranslateService,
				public _cookieService:CookieService,
				public ChangePwdService: ChangePasswordServices,
				private flashMessagesService: FlashMessagesService) {
		this.changePwdForm = this.fb.group({
											old_password: ['', Validators.compose([Validators.minLength(6), Validators.required])],
											new_password: ['', Validators.compose([Validators.minLength(6), Validators.required])],
											confirm_password: ['', Validators.compose([Validators.minLength(6), Validators.required])],
							}, { validator: this.MatchPassword });
		this.isloader=false;
	}
	ngOnInit() {}

	/**
	 * Match Password Function to reset password. 
	 * @access public
	 * @param : AC AbstractControl
	 */
	MatchPassword(AC: AbstractControl) {
		const newPassword = AC.get('new_password').value // to get value in input tag
		const confirmPassword = AC.get('confirm_password').value // to get value in input tag
		if (newPassword != confirmPassword) {
			AC.get('confirm_password').setErrors({ MatchPassword: true });
		} else {
			AC.get('confirm_password').setErrors(null);
			AC.get('confirm_password').setValidators(Validators.compose([Validators.pattern('^.*(?=.{5,15})[a-zA-Z0-9]*$'), Validators.required]));
			AC.get('confirm_password').updateValueAndValidity({ emitEvent: true, onlySelf: true });
		}
	}

	/**
	 * change Password Function to reset password. 
	 * @access public
	 * @param : Form data
	 * @param : Form reference
	 */
	changePwd(formData:any, form:NgForm){
		this.isloader=true;
		let formdata: FormData = new FormData();
		formdata.append('old_password', formData.old_password);
		formdata.append('new_password', formData.new_password);
		formdata.append('confirm_password', formData.confirm_password);
		this.ChangePwdService.changePassword(formdata)
						.subscribe(
							response=> {
								setTimeout(()=> {
									if(response['status']=='success'){
										this.closeChangePwd.nativeElement.click();
									}
									this.isloader=false;
								}, 500);
							}
						);
	}

	/**
	 * Login Function 
	 * @access public
	 * @param : forgot Modal form reference
	 */
	closeForm(changePwdModal:NgForm){
		changePwdModal.resetForm();
	}
}
