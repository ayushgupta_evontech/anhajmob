export interface Login{    
    email:string,
    password:string,
    user_type:string,
    access_token :string,
}