import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SignInComponent } from "./sign-in.component";
import { SignInRouter } from './sign-in.router';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
	declarations: [],
	imports: [
		SignInRouter,
		CommonModule,
		FormsModule,
		ReactiveFormsModule,
		TranslateModule	
	],
	exports:[]	
})

export class SignInModule { }