import { Component ,OnInit, EventEmitter, ViewChild, ElementRef, Output} from '@angular/core';
import { Router, Event as RouterEvent} from '@angular/router';
import { NgForm, FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { FlashMessagesService } from 'ngx-flash-messages';

import { Login } from './model/login.model';
import { AuthServices } from '../services/auth.service';
import { CookieService } from 'ngx-cookie-service';
import { TranslateService } from '@ngx-translate/core';

@Component({
	selector: 'app-sign-in',
	templateUrl: './sign-in.component.html',
	providers:[]
})
export class SignInComponent implements OnInit {
	
	signInForm: FormGroup;
	logins: Login[];
	isloader:boolean;
	isRemember:boolean;
	@Output() isLogin : EventEmitter <boolean> = new EventEmitter<boolean>();
	@ViewChild('closeSignIn') closeSignIn: ElementRef;
	@ViewChild('signupOpen') signupOpen: ElementRef;
	constructor(private fb: FormBuilder,
				private authService:AuthServices,
				private _cookieService:CookieService,
				private translate:TranslateService,
				private flashMessagesService: FlashMessagesService) {
		this.signInForm = this.fb.group({
			'email': ['', Validators.compose([Validators.required,
							Validators.pattern('^[a-zA-Z0-9][-a-zA-Z0-9._]+@([-a-z0-9]+[.])+[a-z]{2,5}$')])],
			'password': ['', Validators.required]
		});
		this.isloader=false;
		this.isRemember=false;
	}
	ngOnInit() {
		window.scrollTo(0, 0);
	}

	/**
	 * Login Function 
	 * @access public
	 * @param : Form data
	 * @param : Form reference
	 */
	signIn(formData:any, form:NgForm){
		this.isloader=true;
		let formdata: FormData = new FormData();
		formdata.append('email', formData.email);
		formdata.append('password', formData.password);
		this.authService.login(formdata)
						.subscribe(
							response=> {
								let cookieLifeSpan:any='';
								if(response['status']==="success" && this.isRemember){
									cookieLifeSpan=new Date(new Date().getFullYear() + 1, new Date().getMonth(), new Date().getDate());

									localStorage.setItem('userInfo', JSON.stringify(response['data']));
									localStorage.setItem('remember', JSON.stringify(this.isRemember));
									localStorage.setItem('token', response.data.access_token);
								}
								if(response['status']==="success"){									
									this._cookieService.set('userInfo', JSON.stringify(response['data']), cookieLifeSpan);
									this._cookieService.set('remember', JSON.stringify(this.isRemember), cookieLifeSpan);
									this._cookieService.set('token', response.data.access_token, cookieLifeSpan);
									this.closeSignIn.nativeElement.click();

									this.isLogin.emit(true);
									form.reset();
								}
								setTimeout(()=> {
									this.isloader=false;
								}, 500);
							},
							error=> {
								this.isloader=false;	
							},
						)
	}

	/**
	 * Closes Sign-up Modal Function 
	 * @access public
	 * @param : NA
	 */
	signupClick(){
		setTimeout(()=> {
			this.signupOpen.nativeElement.click();
		}, 400);
	}

	/**
	 * Login Function 
	 * @access public
	 * @param : signIn Modal form reference
	 */
	closeForm(signInModal:NgForm){
		signInModal.resetForm();
	}
}
