import { Routes, RouterModule } from '@angular/router';
import { SignInComponent } from "./sign-in.component";

const SIGNIN_ROUTER: Routes = [
    { 
        path: '',
        component: SignInComponent
    }
];

export const SignInRouter = RouterModule.forChild(SIGNIN_ROUTER);