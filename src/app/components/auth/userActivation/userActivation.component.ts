import { Component, AfterViewInit, Input, OnInit, trigger, state, style, transition, animate } from '@angular/core';
import { FormGroup, FormBuilder, FormsModule, Validators, FormControl, } from '@angular/forms';
import { Router, ActivatedRoute, Params, NavigationCancel } from '@angular/router';
import { ViewChild, ElementRef } from '@angular/core';
import { URLSearchParams, } from '@angular/http';
import { NgForm } from '@angular/forms';

import { CookieService } from 'ngx-cookie-service';
import { AuthServices } from '../services/auth.service';
import { TranslateService } from '@ngx-translate/core';

@Component({
	selector: 'app-useractivation',
	templateUrl: './userActivation.component.html',
	providers: [AuthServices, CookieService],
})
export class UserActivationComponent implements OnInit {

	response: any;
	loading = false;
	msg: any;
	isValid: boolean;
	dataArray: string;

	constructor(private authService: AuthServices, 
				private router: Router,
				private translate:TranslateService,
				private _cookieService:CookieService,
				private route: ActivatedRoute) {
			this._cookieService.deleteAll();
	}

	ngOnInit() {
		// subscribe to router event
		let code = this.route.snapshot.queryParams['code'];
		if (!code) {
			//this.router.navigate(['']);
		} else {
			this.checkUserActivation(code);
		}
	}

	checkUserActivation(code: any) {
		this.authService.checkUserActivationKey(code)
			.subscribe(
			response => {
					if (response['status'] == 'success') {
						this.msg = response['data']['message'];
						this.isValid = true;
						setTimeout(() => {
							this.msg = null;
							this.router.navigate(['']);
						}, 4000);
					} else {
						this.msg = response['data']['message'];
						this.isValid = false;
						setTimeout(() => {
							this.msg = null;
							this.router.navigate(['']);
						}, 4000);
					}
			});
	}

	cancelReset() {
		this.router.navigate(['']);
	}



}
