import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { userActivationRouter } from './userActivation.router';
import { UserActivationComponent } from "./userActivation.component";
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
	declarations: [UserActivationComponent],
	imports: [
		userActivationRouter,
		FormsModule,
		ReactiveFormsModule,
		CommonModule,
		TranslateModule
	]
})

export class UserActivationModule { }