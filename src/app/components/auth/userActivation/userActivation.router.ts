import { Routes, RouterModule } from '@angular/router';
import { UserActivationComponent } from "./userActivation.component";

const ACTIVATION_ROUTER: Routes = [
    { 
        path: '',
        component: UserActivationComponent
    }
];

export const userActivationRouter = RouterModule.forChild(ACTIVATION_ROUTER);