import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ResetPasswordComponent } from "./reset-password.component";
import { ResetPasswordRouter } from './reset-password.router';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
	declarations: [],
	imports: [
		ResetPasswordRouter,
		CommonModule,
		FormsModule,
		ReactiveFormsModule,
		TranslateModule	
	],
	exports:[]	
})

export class ResetPasswordModule { }