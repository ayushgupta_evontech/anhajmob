import { Component ,OnInit, ViewChild, ElementRef, Input} from '@angular/core';
import { Router, Event as RouterEvent} from '@angular/router';
import { NgForm, FormGroup, FormBuilder, Validators, FormControl, AbstractControl } from '@angular/forms';
import { FlashMessagesService } from 'ngx-flash-messages';

import { ResetPasswordServices } from './services/reset-password.service';
import { CookieService } from 'ngx-cookie-service';
import { TranslateService } from '@ngx-translate/core';

@Component({
	selector: 'app-reset-password',
	templateUrl: './reset-password.component.html',
	providers:[ResetPasswordServices]
})
export class ResetPasswordComponent implements OnInit {
	
	resetPwdForm: FormGroup;
	isloader:boolean;
	@Input() userMail:string;	
	@ViewChild('closeResetPwd') closeResetPwd: ElementRef;	
	constructor(private fb: FormBuilder,
				private translate:TranslateService,
				private _cookieService:CookieService,
				private resetPwdService: ResetPasswordServices,
				private flashMessagesService: FlashMessagesService) {
		this.resetPwdForm = this.fb.group({
											new_password: ['', Validators.compose([Validators.minLength(6), Validators.required])],
											confirm_password: ['', Validators.compose([Validators.minLength(6), Validators.required])],
							}, { validator: this.MatchPassword });
		this.isloader=false;
	}
	ngOnInit() {}

	/**
	 * Match Password Function to reset password. 
	 * @access public
	 * @param : AC AbstractControl
	 */
	MatchPassword(AC: AbstractControl) {
		const newPassword = AC.get('new_password').value // to get value in input tag
		const confirmPassword = AC.get('confirm_password').value // to get value in input tag
		if (newPassword != confirmPassword) {
			AC.get('confirm_password').setErrors({ MatchPassword: true });
		} else {
			AC.get('confirm_password').setErrors(null);
			AC.get('confirm_password').setValidators(Validators.compose([Validators.pattern('^.*(?=.{5,15})[a-zA-Z0-9]*$'), Validators.required]));
			AC.get('confirm_password').updateValueAndValidity({ emitEvent: true, onlySelf: true });
		}
	}

	/**
	 * Reset Password Function to reset password. 
	 * @access public
	 * @param : Form data
	 * @param : Form reference
	 */
	resetPwd(formData:any, form:NgForm){
		this.isloader=true;
		let formdata: FormData = new FormData();

		formdata.append('userMail', this.userMail);
		formdata.append('new_password', formData.new_password);
		formdata.append('confirm_password', formData.confirm_password);
		this.resetPwdService.resetPassword(formdata)
								.subscribe(
									response=> {
										setTimeout(()=> {
											if(response['data']='success'){
												this.closeResetPwd.nativeElement.click();
											}
											this.isloader=false;
										}, 500);
									}
								);
	}

	/**
	 * Login Function 
	 * @access public
	 * @param : forgot Modal form reference
	 */
	closeForm(resetPwdModal:NgForm){
		resetPwdModal.resetForm();
	}
}
