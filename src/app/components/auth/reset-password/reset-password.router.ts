import { Routes, RouterModule } from '@angular/router';
import { ResetPasswordComponent } from "./reset-password.component";

const ResetPassword_ROUTER: Routes = [
    { 
        path: '',
        component: ResetPasswordComponent
    }
];

export const ResetPasswordRouter = RouterModule.forChild(ResetPassword_ROUTER);