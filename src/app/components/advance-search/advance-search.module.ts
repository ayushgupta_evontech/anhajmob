import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AdvanceSearchComponent } from "./advance-search.component";
import { AdvanceSearchRouter } from './advance-search.router';
import { AdvancesearchResolver } from './resolver/advancesearch-resolver.service';
import { SharedModule } from "../../shared/modules/shared.module";
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
	declarations: [
		AdvanceSearchComponent
	],
	imports: [
		AdvanceSearchRouter,
		CommonModule,
		FormsModule,
		SharedModule,
		ReactiveFormsModule	,
		TranslateModule	
	],
	providers: [
		AdvancesearchResolver
	]
})

export class AdvanceSearchModule { }