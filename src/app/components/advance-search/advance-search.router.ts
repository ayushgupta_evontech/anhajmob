import { Routes, RouterModule } from '@angular/router';
import { AdvanceSearchComponent } from './advance-search.component';
import { AdvancesearchResolver } from './resolver/advancesearch-resolver.service';

const ADVANCESEARCH_ROUTER: Routes = [
    { 
        path: '',
        component: AdvanceSearchComponent,
        resolve: { message: AdvancesearchResolver }
    }
];

export const AdvanceSearchRouter = RouterModule.forChild(ADVANCESEARCH_ROUTER );

