import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl, AbstractControl } from '@angular/forms';
import { NgForm } from '@angular/forms';
import { DataStorageServices } from '../../shared/services/dataStorage.service';
import { ObjNgForPipe } from '../../shared/pipes/ObjNgFor.pipe';
import { Router, ActivatedRoute } from '@angular/router';
import { CookieService } from 'ngx-cookie-service';
import { TranslateService } from '@ngx-translate/core';

/**
 * Advance Search page for filter search.
 *  
 * @export
 * @class AdvanceSearchComponent
 * @implements {OnInit}
 */
@Component({
	selector: 'app-advance-search',
	templateUrl: './advance-search.component.html',
	styleUrls: ['./advance-search.component.css'],
	providers: [ObjNgForPipe]
})

export class AdvanceSearchComponent implements OnInit {
	makes: any=[];
	conditions: any;
	cities: any;
	colors: any;
	models: any;
	interiors: any;
	seats: any;
	kilometres: any;
	countries: any;
	year_from: any;
	year_to: any;
	addAdvanceSearchForm: FormGroup; //Form 
	isNew: boolean = false;
	userInfo:any=[];
	searchObj:any=[];
	trims:any;
	yearFromIdx:string;
	yearToIdx:string;
	constructor(
		//private storageServices:StorageServices,
		private fb: FormBuilder,
		private datastorageServices: DataStorageServices,
		private ObjNgForPipe: ObjNgForPipe,
		private router: Router,
		private route: ActivatedRoute,
		private translate:TranslateService,
		private _cookieService: CookieService
	) {

		this.addAdvanceSearchForm = this.fb.group({
			'yearfrom': ['null'],
			'yearto': ['null'],
			'make_id': [''],
			'model_id': [''],	
			'trim_id': [''],		
			'country_id': [''],
			'condition_id': [''],
			'city_id': [''],
			'price_from': [''],
			'price_to': [''],
			'kilometer': [''],
			'color_id': [''],
			'interiors': [''],
			'seats': ['']

		}, { validator: this.getbetweenValidation });

		//let date: Date = new Date();
		//this.year_from = date.getFullYear();
		
		this.searchObj.city_id='';
		this.searchObj.country_id='';
		this.searchObj.model_id='';
		this.searchObj.make_id='';
		this.searchObj.trim_id='';
		if(this._cookieService.get('userInfo')){
			this.userInfo=JSON.parse(JSON.stringify(this._cookieService.get('userInfo')));
			this.userInfo=JSON.parse(this._cookieService.get('userInfo'));
			this.searchObj.city_id=this.userInfo.city_id;
			this.searchObj.country_id=this.userInfo.country_id;
		}
	}

	ngOnInit() {
		this.datastorageServices.editUser.subscribe(
			data => {
						if(data && data.id){
							this.searchObj.country_id=data.country_id;
							this.searchObj.city_id=data.city_id;
							this.getAllCities(this.searchObj.country_id, false);
						}
		});

		this.route.data.subscribe(data => {
			this.countries = this.ObjNgForPipe.transform(data.message['data']['Country']);
			//this.makes = this.ObjNgForPipe.transform(data.message['data']['Make']);
			this.interiors = data.message['data']['Interior'];
			this.conditions = data.message['data']['condition'];
			this.seats = data.message['data']['seats'];
			this.kilometres = data.message['data']['Kilometer'];
			this.colors = data.message['data']['color'];
			this.year_from = data.message['data']['years'];
		});
	}

	/**
	 * 
	 * @access public
	 * @param AC
	 * @desc function to update validation of year fields. 
	 */
	public getbetweenValidation(AC: AbstractControl) {
		const yearfrom = AC.get('yearfrom').value;
		const yearto = AC.get('yearto').value;

		const pricefrom = AC.get('price_from').value;
		const priceto = AC.get('price_to').value;

		if (yearfrom!='null' && yearto=='null') {
			AC.get('yearto').setErrors({ getYearTo: true });
		} else {
			AC.get('yearto').setErrors(null);
		}

		if (pricefrom && priceto == '') {
			AC.get('price_to').setErrors({ getPriceTo: true });
		} else {
			AC.get('price_to').setErrors(null);
		}
	}

	/**
	 * 
	 * @access public
	 * @param countryid 
	 * @param reset
	 * @desc fetches all cities based on the country with its country id passed.
	 */
	public getAllCities(countryid:any, reset:boolean=true) {
		if(countryid){			
			this.datastorageServices.getCityAPI(countryid)
				.subscribe(
					response => {
						this.cities = this.ObjNgForPipe.transform(response['data']);
						if(reset){
							this.searchObj.city_id='';
						}
					}
				)
		}else{
			this.cities=[];
			this.searchObj.city_id='';
		}
	}

	/**
	 * 
	 * @access public
	 * @param makeid 
	 * @desc fetches all Models based on the make with its make id passed
	 */
	public getAllModels(make_id) {
		this.searchObj.model_id='';
		this.searchObj.trim_id='';
		let formData=new FormData();
		formData.append('yearFrom' , this.yearFromIdx);
		formData.append('yearTo' , this.yearToIdx);
		formData.append('make_id' , make_id);
		this.datastorageServices.getModelAPI(formData)
			.subscribe(
				data => {
					if(data.status=='success'){
						this.models=data['data'];
					}else{
						this.models=[];
					}
				}
			);
	}

	/**
	 * 
	 * @desc As per the year range, get list of models
	 * @access public
	 * @param yearFrom
	 * @param yearTo
	 */
	setMakes(yearFrom:number, yearTo:number){
		this.searchObj.make_id='';
		this.searchObj.model_id='';
		this.searchObj.trim_id='';
		let yearFromIdx = this.year_from.find(x => x.name==yearFrom);
		let yearToIdx = this.year_from.find(x => x.name==yearTo);
		
		this.yearFromIdx=yearFromIdx['id'];
		this.yearToIdx=yearToIdx['id'];

		let formData=new FormData();
		formData.append('yearFrom' , yearFromIdx['id']);
		formData.append('yearTo' , yearToIdx['id']);
		this.datastorageServices.getMakeAPI(formData)
									.subscribe(
										data => {
											if(data.status=='success'){
												this.makes=data['data'];
											}
										}
									);
	}

	/**
	 * 
	 * @desc As per the model id, get list of all trims.
	 * @access public
	 * @param event
	 */
	getAllTrims(make_id:any,model_id:any){
		this.searchObj.trim_id='';
		let formData=new FormData();
		formData.append('yearFrom' , this.yearFromIdx);
		formData.append('yearTo' , this.yearToIdx);
		formData.append('make_id', make_id);
		formData.append('model_id', model_id);
		this.datastorageServices.getTrimsAPI(formData)
									.subscribe(
										data => {
											if(data.status=='success'){
												this.trims=data['data'];
											}else{
												this.trims=[];
											}
										}
									);
	}

	/**
	 * 
	 * @desc As per the search and search query, redirects to show that listing page.
	 * @access public
	 * @param formData
	 * @param form
	 */
	public searchCarData(formData: any, form: NgForm) {
		formData.yearfrom=(formData.yearfrom!='null')? formData.yearfrom : '';
		formData.yearto=(formData.yearto!='null')? formData.yearto : '';
		if (formData['condition_id'] == "1") {
			window.scroll(0, 0);
			this.router.navigate(['/listing/new-listing'], { queryParams: formData });
		}
		else if (formData['condition_id'] == "2") {
			window.scroll(0, 0);
			this.router.navigate(['/listing/used-listing'], { queryParams: formData });
		}
		else {
			window.scroll(0, 0);
			this.router.navigate(['/listing'], { queryParams: formData });
		}

	}

	/**
	 * 
	 * @desc function to set year upto list filter based on yearfrom 
	 * @access public
	 * @param yearfrom 
	 */
	public setYeatTo(yearfrom: any) {
		this.addAdvanceSearchForm.controls['yearto'].setValue('null');
		this.year_to = yearfrom;
	}

	/**
	 * @desc : Restrict user to enter character values at mobile No.
	 * @access public
	 * @param event 
	 */
	public numOnly(event: any) {
		const pattern = /[0-9\+\-\ ]/;
		const inputChar = String.fromCharCode(event.charCode);
		if (!pattern.test(inputChar) && event.charCode != '0') {
			event.preventDefault();
		}
	}

}
