import { Injectable, Inject } from "@angular/core";
import { Resolve,ActivatedRouteSnapshot,RouterStateSnapshot } from '@angular/router';
import { HttpClient,HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Rx';

/**
 * Advance Search Resolver to all dropdown server data.
 * 
 * @export
 * @class AdvancesearchResolver
 * @implements {Resolve}
 */
@Injectable()
export class AdvancesearchResolver implements Resolve<any> {

    constructor(private http: HttpClient) {}
    resolve(
        route: ActivatedRouteSnapshot,
        rstate: RouterStateSnapshot
    ): Observable<any> {
         return this.http.get('vehicles/app_getServerData');
    }

   
}