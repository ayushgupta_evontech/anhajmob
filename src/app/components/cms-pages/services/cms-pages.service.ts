import { Injectable } from '@angular/core'
import { HttpClient,HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import { environment } from "../../../../environments/environment";
import { Request, Response, Headers, RequestOptions } from '@angular/http';

@Injectable()
export class CMSPagesServices {
    constructor(private http: HttpClient) {}

	/**    
    * @function : getAboutUsPageDetails
    * @param NA
    * @description  Get about us page details
    */
    getCMSPagesDetails(page:any): Observable<any> {
        return this.http.get('users/app_getStaticContent/'+page);
    }
}