import { Component, AfterViewInit, Input, OnInit, trigger, state, style, transition, animate } from '@angular/core';
import { FormGroup, FormBuilder, FormsModule, Validators, FormControl, } from '@angular/forms';
import { Router, ActivatedRoute, Params, NavigationCancel } from '@angular/router';
import { ViewChild, ElementRef } from '@angular/core';
import { URLSearchParams, } from '@angular/http';
import { NgForm } from '@angular/forms';
import { DomSanitizer } from '@angular/platform-browser';

import { CookieService } from 'ngx-cookie-service';
import { CMSPagesServices } from './services/cms-pages.service';

/**
 * CMS Pages is to display cms pages based on the page keyword.
 *  
 * @export
 * @class CMSPagesComponent
 * @implements {PipeTransform}
 */
@Component({
	selector: 'app-cms-pages',
	templateUrl: './cms-pages.component.html',
	providers: [CMSPagesServices, CookieService],
})
export class CMSPagesComponent implements OnInit {
	content:any=[];
	page:any;
	constructor(private CMSPagesService: CMSPagesServices, 
				private router: Router,
				private _cookieService:CookieService,
				private route: ActivatedRoute,
				private sanitizer: DomSanitizer) {
			this.page=this.route.snapshot.paramMap.get('id');
			this.getCMSPagesComponent();
	}

	ngOnInit() {}

	/**
	 * @memberof CMSPagesComponent
	 * @method:getCMSPagesComponent()
	 * @param: NA
	 * @requires:NA
	 * @returns:string
	 * @description: Fetch about us details.
	 */
	public getCMSPagesComponent() {
		this.CMSPagesService.getCMSPagesDetails(this.page)
							.subscribe(
										response => {
											if (response['status'] == 'success') {
												let datanew = this.sanitizer.bypassSecurityTrustHtml(response['data']['content']);
												this.content['name']=response['data']['name'];
												this.content['content'] = datanew['changingThisBreaksApplicationSecurity'];
											}
							});
	}
}
