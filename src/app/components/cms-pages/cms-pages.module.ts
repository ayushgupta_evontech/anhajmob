import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CMSPagesRouter } from './cms-pages.router';
import { CMSPagesComponent } from './cms-pages.component';

@NgModule({
	declarations: [CMSPagesComponent],
	imports: [
		CMSPagesRouter,
		CommonModule
	]
})

export class CMSPagesModule { }