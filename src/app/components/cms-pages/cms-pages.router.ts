import { Routes, RouterModule } from '@angular/router';
import { CMSPagesComponent } from './cms-pages.component';

const CMS_ROUTER: Routes = [
    {   
        path: '', 
        component: CMSPagesComponent
    }
];

export const CMSPagesRouter = RouterModule.forChild(CMS_ROUTER);