import { Injectable } from '@angular/core'
import { HttpClient,HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

import { Request, Response, Headers, RequestOptions } from '@angular/http';

@Injectable()
export class HomeServices {
    
    constructor(private http: HttpClient) {}

    /**
	 * @desc Service function to get Admin Info.
	 * @access public
	 */
    public getAdminInfo(): Observable<any> {                
        return this.http.get('users/appgetAdminInfo');
    }    

     /**
	 * @desc Service function to search car at home page. 
	 * @access public
     * @param data
	 */
    public searchCar(data:any): Observable<any> {  
		return this.http.post('users/app_search', data);
	}
}