import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from "./home.component";
import { HomeResolver } from './resolver/home-resolver.service';

const HOME_ROUTER: Routes = [
    { 
        path: '',
        component: HomeComponent,
        resolve: { message: HomeResolver }
    }
];

export const homeRouter = RouterModule.forChild(HOME_ROUTER );

