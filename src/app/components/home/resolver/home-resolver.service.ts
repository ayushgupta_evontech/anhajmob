import { Injectable, Inject } from "@angular/core";
import { Resolve,ActivatedRouteSnapshot,RouterStateSnapshot } from '@angular/router';
import { HttpClient,HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Rx';
import 'rxjs/add/observable/forkJoin';

/**
 * Home Resolver to get server data.
 * 
 * @export
 * @class HomeResolver
 * @implements {Resolve}
 */
@Injectable()
export class HomeResolver implements Resolve<any> {

    constructor(private http: HttpClient) {}

    resolve(
        route: ActivatedRouteSnapshot,
        rstate: RouterStateSnapshot
    ): Observable<any> {
        let response = this.http.get('vehicles/app_getServerData');
        return Observable.forkJoin([response]);       
    }

   
}