import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HomeComponent } from './home.component';
import { homeRouter } from './home.router';
import { TranslateModule } from '@ngx-translate/core';
import { HomeResolver } from './resolver/home-resolver.service';

@NgModule({
	declarations: [
		HomeComponent,
	],
	imports: [
		homeRouter,
		CommonModule,
		FormsModule,
		ReactiveFormsModule,
		TranslateModule		
	],
	providers:[HomeResolver]
})

export class HomeModule { }