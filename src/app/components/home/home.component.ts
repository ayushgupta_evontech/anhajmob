import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { NgForm } from '@angular/forms';

import { HomeServices } from './services/home.service';

import { Router,ActivatedRoute } from '@angular/router';
import { DataStorageServices } from '../../shared/services/dataStorage.service';
import { ObjNgForPipe } from '../../shared/pipes/ObjNgFor.pipe';
import { CookieService } from 'ngx-cookie-service';
import { TranslateService } from '@ngx-translate/core';

/**
 * Landing page Home component
 *  
 * @export
 * @class HomeComponent
 * @implements {OnInit}
 */
@Component({
	selector: 'app-home',
	templateUrl: './home.component.html',
	styleUrls: ['./home.component.css'],
	providers: [HomeServices, ObjNgForPipe]
})


export class HomeComponent implements OnInit {

	countries: any;
	makes: any;
	conditions: any;
	addHomeForm: FormGroup; //Form 
	cities: any;
	models: any;
	isloader:boolean=false;
	isNewCars: boolean;
	userInfo:any=[];
	searchObj:any=[];
	constructor(
		private route: ActivatedRoute,
		private homeServices: HomeServices,
		private datastorageServices: DataStorageServices,
		private fb: FormBuilder,
		private router: Router,
		private translate: TranslateService,		
		private ObjNgForPipe: ObjNgForPipe,
		private _cookieService:CookieService) {
		this.isNewCars = true;
		this.addHomeForm = this.fb.group({
			'make_id': [''],
			'country_id': [''],
			'condition_id': [''],
			'city_id': [''],
			'model_id': ['']

		});
		this.searchObj.city_id='';
		this.searchObj.country_id='';
		this.datastorageServices.editUser.subscribe(
			data => {
						if(data && data.id){
							this.searchObj.country_id=data.country_id;
							this.searchObj.city_id=data.city_id;
							this.getAllCities(this.searchObj.country_id, false);
						}else{
							this.searchObj.city_id='';
							this.searchObj.country_id='';
						}						
		});
	}

	ngOnInit() {
		window.scroll(0,0);
		this.route.data.subscribe(data => {
			this.makes=this.ObjNgForPipe.transform(data.message[0]['data']['Make']);
			this.conditions=data.message[0]['data']['condition'];
			this.countries=this.ObjNgForPipe.transform(data.message[0]['data']['Country']);
		});
	}

	/**
	 * 
	 * @access public
	 * @param countryid 
	 * @param reset
	 * @desc fetches all cities based on the country with its country id passed
	 */
	public getAllCities(countryid:any, reset:boolean=true) {
		if(countryid){
			this.datastorageServices.getCityAPI(countryid)
				.subscribe(
					response => {
						this.cities = this.ObjNgForPipe.transform(response['data']);
						if(reset){
							this.searchObj.city_id='';						
						}
					}
			);
		}else{
			this.cities=[];
			this.searchObj.city_id='';
		}
	}

	/**
	 * 
	 * @access public
	 * @param makeid 
	 * @desc fetches all Models based on the make with its make id passed
	 */
	public getAllModels(makeid) {
		if(makeid){
			let formData=new FormData();
			formData.append('make_id', makeid);
			this.datastorageServices.getModelAPI(formData)
				.subscribe(
					data => {
						this.models = data['data'];
					}
				);
		}else{
			this.models=[];
			this.searchObj.model_id='';
		}
	}

	/**
	 * @desc sets boolean value of new & used car tabs condition.  
	 */
	public isActiveTab() {
		this.isNewCars = !this.isNewCars;
	}

	/**
	 * 
	 * @access public
	 * @param formData 
	 * @param form
	 * @desc As per the search and search query, redirects to show that listing page
	 */
	public searchCarData(formData: any, form: NgForm) {
		this.isloader=true;
		formData['condition_id'] = this.isNewCars ? '1' : '2';
		formData['page'] = 1;
		if (formData['condition_id'] == "1") {
				setTimeout(()=> {
					this.isloader=false;	
				}, 500);
				this.router.navigate(['/listing/new-listing'], { queryParams: formData });
		} else {
				setTimeout(()=> {
					this.isloader=false;	
				}, 500);
				this.router.navigate(['/listing/used-listing'], { queryParams: formData });
		}
	}
}