import { Routes, RouterModule } from '@angular/router';
import { NotfoundComponent } from "../notfound/notfound.component";

const NOTFOUND_ROUTER: Routes = [
    { 
        path: '',
        component: NotfoundComponent
    }
];

export const NotFoundRouter = RouterModule.forChild(NOTFOUND_ROUTER);

