import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';

/**
 * On random route the component is called
 * to display user the static 404 not found page.
 * 
 * @export
 * @class NotfoundComponent
 * @implements {OnInit}
 */
@Component({
  selector: 'app-notfound',
  templateUrl: './notfound.component.html',
})
export class NotfoundComponent implements OnInit {

  constructor(private translate:TranslateService) { }

  ngOnInit() {
  }

}

