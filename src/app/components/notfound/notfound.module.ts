import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule,ReactiveFormsModule } from '@angular/forms';
import { NotFoundRouter } from './notfound.router';
import { NotfoundComponent } from "../notfound/notfound.component";
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  declarations: [
    NotfoundComponent
  ],
  imports: [ 
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    NotFoundRouter,
    TranslateModule
  ]
})

export class NotFoundModule {}