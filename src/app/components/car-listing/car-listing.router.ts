import { Routes, RouterModule } from '@angular/router';
import { CarListingComponent } from "./car-listing.component";
import { CarListingResolver } from './resolver/carlisting-resolver.service';
import { CarEditgResolver } from './resolver/caredit-resolver.service';
import { CarEditComponent } from "./car-edit/car-edit.component";
import { SellcarResolver } from '../../shared/resolver/sellcar-resolver.service';

const CARLISTING_ROUTER: Routes = [
    { 
        path: '',
        component: CarListingComponent,
        resolve: { message: CarListingResolver },
        data: { state: 'mycars' }
    },{ 
        path: 'car-edit/:id',
        component: CarEditComponent,
        resolve: { message: CarEditgResolver },
        data: { state: 'caredit' }
    }
];

export const CarListingRouter = RouterModule.forChild(CARLISTING_ROUTER );

