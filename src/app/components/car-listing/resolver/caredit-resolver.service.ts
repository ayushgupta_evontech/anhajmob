import { Injectable, Inject } from "@angular/core";
import { Resolve,ActivatedRouteSnapshot,RouterStateSnapshot, ActivatedRoute } from '@angular/router';
import { HttpClient,HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Rx';
import 'rxjs/add/observable/forkJoin';
import { CookieService } from 'ngx-cookie-service';

/**
 * Car Editg Resolver to get Makes, Conditionsm, Colors & Vehicle.
 * 
 * @export
 * @class CarEditgResolver
 * @implements {Resolve}
 */
@Injectable()
export class CarEditgResolver implements Resolve<any> {

    constructor(private http: HttpClient,
                private _cookieservice:CookieService) {}
    
    resolve(
        route: ActivatedRouteSnapshot,
        rstate: RouterStateSnapshot
    ): Observable<any> {
        let carId:any=+route.params['id'];
        
        let response1 = this.http.get('vehicles/app_getAllMakes');
        let response2 = this.http.get('vehicles/app_getAllConditions');
        let response3 = this.http.get('vehicles/app_getAllColors');
        let response4 = this.http.get('vehicles/app_getVehicleById/'+carId)
        let response5 = this.http.get('vehicles/app_getServerData');        
        return Observable.forkJoin([response1, response2, response3, response4, response5]);
    }

   
}