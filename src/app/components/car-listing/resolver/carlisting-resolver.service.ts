import { Injectable, Inject } from "@angular/core";
import { Resolve,ActivatedRouteSnapshot,RouterStateSnapshot } from '@angular/router';
import { HttpClient,HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Rx';
import 'rxjs/add/observable/forkJoin';
import { CookieService } from 'ngx-cookie-service';
/**
 * Car Listing Resolver to get Makes & car search result.
 * 
 * @export
 * @class CarListingResolver
 * @implements {Resolve}
 */
@Injectable()
export class CarListingResolver implements Resolve<any> {

    constructor(private http: HttpClient,
                private _cookieservice:CookieService) {}
    resolve(
        route: ActivatedRouteSnapshot,
        rstate: RouterStateSnapshot
    ): Observable<any> {        
        let userInfo :any;
        userInfo = JSON.parse(JSON.stringify(this._cookieservice.get('userInfo')));
        userInfo = JSON.parse(userInfo);
     
        let searchObj:any = {};
        searchObj.make_id = route.queryParams['make_id'];
        searchObj.model_id = route.queryParams['model_id'];
        searchObj.user_id = userInfo.id;
		searchObj.page = route.queryParams['page'];
        let response1 = this.http.get('vehicles/app_getAllUsersMakes');
        let response2 = this.http.post('vehicles/app_mycarsearchResult',searchObj);
        
        return Observable.forkJoin([response1, response2]);
    }   
}