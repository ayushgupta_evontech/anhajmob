import { Injectable } from '@angular/core'
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import { Request, Response, Headers, RequestOptions } from '@angular/http';

@Injectable()
export class CarlistingServices {

	constructor(private http: HttpClient) { }
	
	/**
	 * @desc Service Function to get search listing
	 * @access public
	 * @param data
	 */
	public getSearchListing(data: any): Observable<any> {
		return this.http.post('vehicles/app_mycarsearchResult', data);
	}

	/**
	 * @desc Service Function to delete vehicle
	 * @access public
	 * @param vehicleId
	 */
	public deleteVehicle(vehicleId: any): Observable<any> {
		return this.http.get('vehicles/app_deleteVehicle/' + vehicleId);
	}
	
	/**
	 * @desc Service Function to edit vehicle
	 * @access public
	 * @param data
	 */
	public editCar(data: any): Observable<any> {
		return this.http.post('vehicles/app_editVehicle', data);
	}

	/**
	 * @desc Service Function to delete vehicle files.
	 * @access public
	 * @param id
	 */
    public deleteVehicleFiles(id: any): Observable<any> {
        let body = { id: id };       
        return this.http.post('vehicles/app_deleteVehicleFile', body);
	}
	
	/**
	 * @desc Service Function to delete vehicle tags.
	 * @access public
	 * @param tagId
	 */
    public deleteVehicletags(tagId: any): Observable<any> {
        let body = { id: tagId };       
        return this.http.post('vehicles/app_deleteVehicletags', body);
	}
	
	/**
	 * @desc Service Function to get Vin info from third party api.
	 * @access public
	 * @param id
	 */
	public getVinInfo(id:any): Observable<any> {
		return this.http.get('https://vpic.nhtsa.dot.gov/api/vehicles/DecodeVinValues/'+id+'?format=json');
	}
}