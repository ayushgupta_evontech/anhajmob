import { Component, OnInit, Input, Output, ViewChild, ElementRef, EventEmitter } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { DataStorageServices } from '../../shared/services/dataStorage.service';
import { CarlistingServices } from './services/carlisting.service';
import { CookieService } from 'ngx-cookie-service';
import { environment } from "../../../environments/environment";
import { ObjNgForPipe } from '../../shared/pipes/ObjNgFor.pipe';
import { TranslateService } from '@ngx-translate/core';
import { animateList } from '../../shared/animation/animation';

/**
 * Car Listing Page to displays list of dealers cars.
 * 
 * @export
 * @class CarListingComponent
 * @implements {OnInit}
 */
@Component({
	selector: 'app-car-list',
	templateUrl: './car-listing.component.html',
	styleUrls: ['./car-listing.component.css'],
	providers: [CarlistingServices, ObjNgForPipe],
	animations: [animateList],
})

export class CarListingComponent implements OnInit {

	isFillter: boolean;
	searchData: any = [];
	countSearch: any=0;
	makes: any;
	models: any;
	myCarForm: FormGroup; //Form 
	currentPage: any=1;
	searchObj: any = {};
	baseUrl: any = environment.baseUrl;
	collection = [];
	wasClicked = false;
	rmVehicleId:any;
	isFullListDisplayed:boolean= false;
	@ViewChild('closetagBtn') closetagBtn: ElementRef;
	constructor(private route: ActivatedRoute,
		private fb: FormBuilder,
		private datastorageServices: DataStorageServices,
		private carlistingServices: CarlistingServices,
		private _cookieservice: CookieService,
		private translate:TranslateService,
		private objNgForPipe : ObjNgForPipe) {
		this.myCarForm = this.fb.group({
			'make_id': [''],
			'model_id': ['']
		});
		this.isFillter = true;
	}

	ngOnInit() {
		this.searchObj.page=1;
		this.route.data.subscribe(data => {
			if(data.message[0]['status']=='success'){
				this.makes = data.message[0]['data'];
			}
			if (data.message[1].status == 'success' && data.message[1]['count'] > 0) {
				this.searchData = data.message[1]['data'];
				this.countSearch = data.message[1]['count'];
			}
		});
	}

	/**
	 * 
	 * @access public
	 * @desc on click of the listing image sets boolean condition. 
	 */
	public onClick() {
		this.wasClicked = !this.wasClicked;
	}

	/**
	 * 
	 * @access public
	 * @param formData
	 * @desc function to filter the listing result.
	 */
	public myCarData(formData: any) {
		if (this.searchObj.make_id != formData['make_id'] || formData['model_id'] == "") {
			formData['model_id']="";
			this.getAllModels(formData['make_id']);
		}
		this.searchObj = formData;
		this.searchObj.page=1;
		let userInfo: any;
		userInfo = JSON.parse(JSON.stringify(this._cookieservice.get('userInfo')));
		userInfo = JSON.parse(userInfo);
		formData['user_id'] = userInfo.id;
		this.searchData = [];
		this.carlistingServices.getSearchListing(formData)
			.subscribe(
			data => {
				this.countSearch = 0;
				if (data['status'] == "success") {
					this.searchData = data['data'];
					this.countSearch = data['count'];
				}
			}
			)
	}

	/**
	 * 
	 * @access public
	 * @param makeid 
	 * @desc fetches all Models based on the make with its make id passed
	 */
	public getAllModels(makeid) {
		if (makeid) {
			this.myCarForm.get('model_id').setValue('');
			let formdata = new FormData();
			formdata.append('makeId', makeid);
			this.datastorageServices.getUserModelAPI(formdata)
				.subscribe(
				data => {
					this.models = data['data'];
				}
				)
		} else {
			this.models = [];
		}
	}

	/**
	 * 
	 * @access public
	 * @param event 
	 * @desc fetches all searver data like make, model, country etc.
	 */
	// public getServerData(event) {
	// 	this.searchObj.page = event;
	// 	this.currentPage = event;
	// 	this.carlistingServices.getSearchListing(this.searchObj)
	// 							.subscribe(
	// 								response => {
	// 									if (response.error) {
	// 										console.log('Server Error');
	// 									} else {
	// 										this.searchData = response.data;
	// 									}
	// 								}
	// 							);
	// 	return event;
	// }

	/**
	 * 
	 * @access public
	 * @param id 
	 * @desc sets deleting vehicle id.
	 */
	public deleteVehicle(id:any=null) {
		this.rmVehicleId=id;
	}

	/**
	 * 
	 * @access public
	 * @param isAgreed 
	 * @desc Deletes vehicle based on vehicle id and isAgreed condition.
	 */
	public checkRmVehicle(isAgreed:boolean){
		if (isAgreed && this.rmVehicleId) {
			let idx=this.searchData.findIndex(x=>parseInt(x.id)==parseInt(this.rmVehicleId));			
			this.carlistingServices.deleteVehicle(this.rmVehicleId)
				.subscribe(
				response => {
					if (response['status'] == "success") {
						/* let formdata: any = {};
						formdata.make_id = '';
						formdata.model_id = '';
						this.myCarData(formdata);*/
						this.searchData.splice(idx, 1);
												
					}
				}
				)
		}else{
			this.rmVehicleId=null;
		}
		this.closetagBtn.nativeElement.click();
	}

	onScrollDown() {
		this.searchObj.page += this.currentPage;
		if(this.searchObj.page > 1){
			this.carlistingServices.getSearchListing(this.searchObj)
									.subscribe(
										response => {
											if (response.error) {
												console.log('Server Error');
											} else if(response.data.length > 0) {
												response.data.forEach(item => {
													this.searchData.push(item);	
												});
											}
										}
									);
		}
	}
	 
	onScrollUp() {
		console.log('scrolled up!!');
	}
}
