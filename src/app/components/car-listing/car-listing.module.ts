import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CarListingComponent } from "./car-listing.component";
import { CarListingRouter } from './car-listing.router';
import { CarListingResolver } from './resolver/carlisting-resolver.service';
import { CarEditgResolver } from './resolver/caredit-resolver.service';
import { Ng5SliderModule } from 'ng5-slider';
import { NgxPaginationModule } from 'ngx-pagination';
import { CarEditComponent } from "./car-edit/car-edit.component";
import { SharedModule } from "../../shared/modules/shared.module";
import { TagInputModule } from 'ngx-chips';
import { TranslateModule } from '@ngx-translate/core';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';

@NgModule({
	declarations: [
		CarListingComponent,
		CarEditComponent
	],
	imports: [
		CarListingRouter,
		CommonModule,
		FormsModule,
		Ng5SliderModule, 
		NgxPaginationModule,
		ReactiveFormsModule,
		SharedModule,
		TagInputModule,
		TranslateModule,
		InfiniteScrollModule
	],
	providers: [
		CarListingResolver,
		CarEditgResolver
	]
})

export class CarListingModule { }