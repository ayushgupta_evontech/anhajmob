import { Component, OnInit, Input, Output, EventEmitter, ViewChild, ElementRef} from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl, } from '@angular/forms';
import { NgForm } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { DataStorageServices } from '../../../shared/services/dataStorage.service';
import { ObjNgForPipe } from '../../../shared/pipes/ObjNgFor.pipe';
import { FlashMessagesService } from 'ngx-flash-messages';

import { CarlistingServices } from '../services/carlisting.service';
import { CookieService } from 'ngx-cookie-service';
import { environment } from '../../../../environments/environment'
import { TranslateService } from '@ngx-translate/core';
declare var window: any;

/**
 * Edit Car Page to update vehicle.
 * 
 * @export
 * @class CarEditComponent
 * @implements {OnInit}
 */
@Component({
	selector: 'app-car-edit',
	templateUrl: './car-edit.component.html',
	styleUrls: ['./car-edit.component.css'],
	providers: [CarlistingServices, ObjNgForPipe,CookieService]
})

export class CarEditComponent implements OnInit {
	countries: any;
	cities: any;
	makes: any;
	models: any;
	conditions: any;
	colors: any;
	documentsArray: any;
	files: any = [];
	isloader:boolean;
	addSellcarForm: FormGroup; //Form 
	imageLength:any;
	maxImageError:any;
	userInfo:any=[];
	carLimit:number;
	carCount:number;
	isShow:boolean=true;
	year_from:any;
	vehicle:any=[];
	imageArray:any=[];
	setDeleteDocId: any;
	setDeleteDocIndex: any;
	removeTag:any;
	trims:any=[];
	modelBody:any=[];
	engineSize:any=[];
	engineCylinders:any=[];
	horsepower:any=[];
	fuelType:any=[];
	doors:any=[];
	seat:any=[];
	isValidVIN:boolean=false;
	baseUrl:any=environment.baseUrl;
	@ViewChild('closeImgBtn') closeImgBtn: ElementRef;
	@ViewChild('closetagBtn') closetagBtn: ElementRef;
	@ViewChild('deletetagbtn') deletetagbtn: ElementRef;
	@ViewChild('vin') vin: ElementRef;
	constructor(
		private fb: FormBuilder,
		private route: ActivatedRoute,
		private router: Router,
		private ObjNgForPipe: ObjNgForPipe,
		private _cookieservice:CookieService,
		private translate:TranslateService,
		private datastorageServices: DataStorageServices,
		private flashMessagesService:FlashMessagesService,
		private carlistingService: CarlistingServices
	) {

		this.addSellcarForm = this.fb.group({
			'make_id': ['', Validators.required],
			'model_id': ['', Validators.required],
			'yearfrom': ['', Validators.required],
			'trim_id': ['', Validators.required],
			'model_body': [''],
			'engine_size': [''],
			'engine_cylinders' : [''],
			'horsepower': [''],
			'fuel_type': [''],
			'doors': [''],
			'country_id': ['', Validators.required],
			'city_id': ['', Validators.required],
			'price': ['', Validators.required],
			'kilometer': ['', Validators.required],
			'mileagetype': ['km', Validators.required],			
			'condition_id': ['', Validators.required],
			'color_id': ['', Validators.required],
			'seat_id': ['', Validators.required],
			'vin': ['', Validators.required],
			'interior_id': [''],
			'video': [''],
			'description': [''],
			'fileInput': [''],
			'is_active': ['', Validators.required]
		});
		this.isloader=false;
		this.userInfo = JSON.parse(JSON.stringify(this._cookieservice.get('userInfo')));
		this.userInfo=JSON.parse(this.userInfo);

		// let date:Date=new Date();
		// this.year_from=date.getFullYear();
	}

	ngOnInit() {
		this.datastorageServices.countries.subscribe(
			response=>{
				this.countries=response;
		});
		this.route.data.subscribe(data => {
			// this.makes = this.ObjNgForPipe.transform(data.message[0]['data']);
			this.conditions = data.message[1]['data'];
			this.colors = data.message[2]['data'];
			this.vehicle=data.message[3]['data'];

			this.vehicle.doors=(data.message[3]['data']['model_doors_id'])?data.message[3]['data']['model_doors_id']:'';
			this.vehicle.engine_cylinders=(data.message[3]['data']['model_engine_cylinders_id'])?data.message[3]['data']['model_engine_cylinders_id']:'';
			this.vehicle.engine_size=(data.message[3]['data']['model_engine_cc_id'])?data.message[3]['data']['model_engine_cc_id']:'';
			this.vehicle.fuel_type=(data.message[3]['data']['model_engine_fuel_id'])?data.message[3]['data']['model_engine_fuel_id']:'';
			this.vehicle.horsepower=(data.message[3]['data']['model_engine_power_ps_id'])?data.message[3]['data']['model_engine_power_ps_id']:'';
			this.vehicle.model_body=(data.message[3]['data']['model_body_id'])?data.message[3]['data']['model_body_id']:'';
			this.vehicle.seats=data.message[3]['data']['seat_id'];
			this.vehicle.is_active=data.message[3]['data']['is_active'];

			this.modelBody = data.message[4]['data']['model_body'];
			this.engineSize = data.message[4]['data']['engine_size'];
			this.engineCylinders = data.message[4]['data']['engine_cylinders'];
			this.horsepower = data.message[4]['data']['horsepower'];
			this.fuelType = data.message[4]['data']['fuel_type'];
			this.fuelType = data.message[4]['data']['fuel_type'];
			this.doors= data.message[4]['data']['doors'];
			this.seat= data.message[4]['data']['seats'];
			this.year_from=data.message[4]['data']['years'];
			
			if(this.vehicle.vehicle_images.length > 0 && this.vehicle.vehicle_images[0].type==1){
				let token=this.vehicle.vehicle_images[0].name.split('https://www.youtube.com/embed/');
				this.vehicle.video=token[1];
			}
			this.imageArray = this.vehicle.vehicle_images.filter(x=>x.type==0);
			this.getAllCities(this.vehicle.country_id);
			this.setMakes(this.vehicle.yearfrom, '');
			this.getAllModels(this.vehicle.make_id, '');
			this.getAllTrims(this.vehicle.model_id, '');
			this.vehicle.interior_id=[];
			this.vehicle.vehicle_interiors.map((obj, i) => this.vehicle.interior_id.push({display: obj.name,
			value: obj.name}));		
		});
	}

	/**
	 * 
	 * @access public
	 * @param countryid
	 * 
	 * @desc fetches all cities based on the country with its country id passed.
	 */
	public getAllCities(countryid) {
		if(countryid){
			this.datastorageServices.getCityAPI(countryid)
				.subscribe(
					data => {
						this.cities=this.ObjNgForPipe.transform(data['data']);
					}
				)
		}else{
			this.cities=[];
			this.vehicle.city_id='';
		}
	}

	/**
	 * 
	 * @desc on sell car form submit saves vehicels.
	 * @access public
	 * @param formData
	 * @param form
	 */
	public sellCarData(formData: any, form: NgForm) {
		this.isloader=true;
		let index;
		let fileArray = new FormData();
		this.files.map((item, i) => fileArray.append("MyFile[" + i + "]", item));
		if(formData['interior_id'].length >0){			
			formData['interior_id'].map((obj, j) =>{ 
				let index= this.vehicle.vehicle_interiors.findIndex(x=>x.name==obj.value);
				if (index == -1) {
					fileArray.append("vehicle_interiors[" + j + "][name]", obj.value);
				}		
			});
		}
		fileArray.append("id", this.vehicle.id);
		fileArray.append("user_id", this.userInfo.id);
		fileArray.append("make_id", formData['make_id']);
		fileArray.append("model_id", formData['model_id']);
		fileArray.append("country_id", formData['country_id']);
		fileArray.append("city_id", formData['city_id']);
		fileArray.append("price", formData['price']);
		fileArray.append("kilometer", formData['kilometer']);
		fileArray.append("mileagetype", formData['mileagetype']);		
		fileArray.append("condition_id", formData['condition_id']);
		fileArray.append("color_id", formData['color_id']);
		fileArray.append("trim_id", formData['trim_id']);
		fileArray.append("model_body_id", formData['model_body']);
		fileArray.append("model_engine_cc_id", formData['engine_size']);
		fileArray.append("model_engine_cylinders_id", formData['engine_cylinders']);
		fileArray.append("model_engine_power_ps_id", formData['horsepower']);
		fileArray.append("model_engine_fuel_id", formData['fuel_type']);
		fileArray.append("model_doors_id", formData['doors']);		
		fileArray.append("seat_id", formData['seat_id']);
		fileArray.append("vin", formData['vin']);
		fileArray.append("yearfrom", formData['yearfrom']);
		fileArray.append("description", formData['description']);
		fileArray.append("is_active", formData['is_active']);
		if(formData['video']){
			fileArray.append("video", formData['video']);
		}

		this.carlistingService.editCar(fileArray)
								.subscribe(
									response => {
												if(response['status']=='success'){
													this.router.navigate(['car-listing']);
												}
												setTimeout(()=> {
													this.isloader=false;	
												}, 550);
								}
							);
	}

	/**
	 * 
	 * @desc set deleting document id & index 
	 * @access public
	 * @param id
	 * @param index
	 */
	public setDeleteUserDoc(id: any, index: any) {
		//Set details which going to be delete
		this.setDeleteDocId = id;
		this.setDeleteDocIndex = index;
	}

	/**
	 * 
	 * @desc deleting document from the database.
	 * @access public
	 * @param isAgree
	 */
	public deleteUserDoc(isAgree: any) {
		if (isAgree == 'true') {
			let docID = this.setDeleteDocId; //Value comming from  setDeleteImageVal()
			let index = this.setDeleteDocIndex; //Value comming from  setDeleteImageVal()
			this.closeImgBtn.nativeElement.click();

			this.carlistingService.deleteVehicleFiles(docID)
				.subscribe(
				data => {
						if (data['status'] == 'success') {
							if (index > -1) {
								this.imageArray.splice(index, 1);
							}
							this.setDeleteDocId = null;
							this.setDeleteDocIndex = null;
						}
					}
				)
		} else {
			this.setDeleteDocId = null;
			this.setDeleteDocIndex = null;
		}
	}

	/**
	 * 
	 * @desc  sets deleting tag of interior design.
	 * @access public
	 * @param event
	 */
	public ontagRemoved(event){
		if(this.vehicle.vehicle_interiors.length > 0){
			if(event){
				let tag=this.vehicle.vehicle_interiors.find(x=>x.name==event.value);
				if(tag){
					this.removeTag=tag;
					this.deletetagbtn.nativeElement.click();				
				}
			}
		}
	}

	/**
	 * 
	 * @desc deletes tag of interior design.
	 * @access public
	 * @param isAgree
	 */
	public checkTagRemoved(isAgree: any){
		if(this.vehicle.vehicle_interiors.length > 0){			
			if (isAgree == true) {
				this.carlistingService.deleteVehicletags(this.removeTag['id'])
					.subscribe(
					data => {
							if (data['status'] == 'success') {
								this.removeTag=null;
								this.closetagBtn.nativeElement.click();
							}
						}
					);
			} else if (isAgree == false && this.removeTag) {
				this.vehicle.interior_id.push({display : this.removeTag.name, value : this.removeTag.name });
				this.removeTag=null;
			}
		}
	}

	///add sell car documents
	/**
	 * 
	 * @desc Add vehicle documents
	 * @access public
	 * @param event
	 * @param imgTag
	 */
	public addVehicleImages(event, imgTag) {
		let j = 0;
		let thisObj = this;
		let data: any;
		let filenumber: number;
		let imageExt = ['IMAGE/JPEG', 'IMAGE/JPG', 'IMAGE/PNG', 'image/jpeg', 'image/jpg', 'image/png'];
		let k = 0;
		let imgs = document.getElementById('imageID').getElementsByClassName('multipleImageWrap');
		let imageLength = (5 - imgs.length);

		// if (event.target.files.length > imageLength) {
		// 	this.maxImageError = "You can't upload more then 5 images";
		// } else {
		if (event.target.files.length > 0) {		
			this.documentsArray = event.target.files;
			let files:any=[].slice.call(event.target.files);
			this.files =this.files.concat(files);

			//using of map by kshitiz//
			let myres = files.map((product, i) => {
				//return product.name;
				let ext=product.type;
				if(imageExt.indexOf(ext)!=-1){
					let j = i;
					let reader = new FileReader();
					reader.onloadend = function () {
						let binaryString = reader.result;
						let img = document.createElement("img");
						img.src = binaryString;

						//now goind gor delete images
						let div = document.createElement("div");
						let idx: any = j;
						j++;
						div.setAttribute("id", idx);
						div.setAttribute("class", "clearfix pos_rel divImagesWrap");
						div.innerHTML = "<img class='thumbnail' src='" + binaryString + "'" +
							"title='hllo'/> <img src='assets/images/delete.png' class='remove_pict' style='position:absolute;right: -7px;bottom:-4px;height:24px;width:24px;'>";
						imgTag.appendChild(div);
						div.children[1].addEventListener("click", (event) => {
							let index: any = div.id;
							index = parseInt(index);
							thisObj.removeFile(i);
							j--;
							div.parentNode.removeChild(div);
						}, false);
						//now going  for delete images
					}
					reader.readAsDataURL(product);
				}
			});
			//using of map ends//
		}
	}


	/**
	 * 
	 * @desc remove vehicle document
	 * @access public
	 * @param index
	 */
	public removeFile(index) {
		this.files.splice(index, 1);
	}
	//add ends

	/**
	* 
	* @access public
	* @param event
    * @desc  Restrict user to enter character values at mobile No.
    */
	numOnly(event: any) {
		const pattern = /[0-9\+\-\ ]/;
		const inputChar = String.fromCharCode(event.charCode);
		if (!pattern.test(inputChar) && event.charCode != '0') {
			event.preventDefault();
		}
	}

	/**
	 * 
	 * @access public
	 * @param vin
	 * @desc function to vin information from the third party api.
	 */
	public getVinInfo(vin) {
		if (vin) {
			this.carlistingService.getVinInfo(vin)
				.subscribe(
				data => {
					let err: any = data['Results'][0]['ErrorCode'].split(' ');
					if (err[0] == 0) {
						let makeObj = this.makes.find(x => x.key.toLowerCase() == data['Results'][0]['Make'].toLowerCase());
						this.vehicle.make_id = makeObj.id;
						this.getAllModels(makeObj.id, data);
						this.vehicle.yearfrom = (data['Results'][0]['ModelYear']) ? data['Results'][0]['ModelYear'] : this.vehicle.yearfrom;
						this.vehicle.seats = (data['Results'][0]['Seats']) ? data['Results'][0]['Seats'] : this.vehicle.seats;
						this.vehicle.price = (data['Results'][0]['BasePrice']) ? data['Results'][0]['BasePrice'] : this.vehicle.price;
						this.vehicle.description = (data['Results'][0]['OtherRestraintSystemInfo']) ? data['Results'][0]['OtherRestraintSystemInfo'] : this.vehicle.description;
						this.vehicle.country_id = (data['Results'][0]['Country']) ? data['Results'][0]['Country'] : this.vehicle.country_id;
						this.getAllVinCities(this.vehicle.country_id, data['Results'][0]['City']);
						let engineCylindersIdx=this.engineCylinders.find(x => x.name == data['Results'][0]['EngineCylinders']);
						let horsepowerIdx=this.horsepower.find(x => x.name == data['Results'][0]['EngineHP']);
						let fuelTypeIdx=this.fuelType.find(x => x.name==data['Results'][0]['FuelTypePrimary']);
						let doorsIdx=this.doors.find(x => x.name==data['Results'][0]['Doors']);

						this.vehicle.engine_cylinders=engineCylindersIdx.id;
						this.vehicle.horsepower=(horsepowerIdx)?horsepowerIdx.id:'';
						this.vehicle.fuel_type=(fuelTypeIdx)?fuelTypeIdx.id:'';
						this.vehicle.doors=(doorsIdx)?doorsIdx.id:'';
						this.setMakes(this.vehicle.yearfrom, data);
						this.isValidVIN=false;
					}else{
						this.isValidVIN=true;
						this.vehicle.yearfrom = '';
						this.vehicle.make_id = '';
						this.vehicle.model_id = '';
						this.vehicle.description = '';
						this.vehicle.price = '';
						this.vehicle.seats = '';
					}
				}
				)
		}
	}

	/**
	 * 
	 * @access public
	 * @param countryid
	 * @param data
	 * @desc fetches all cities based on the vin country with its country id passed.
	 */
	public getAllVinCities(countryid, data) {
		if (countryid && data) {
			this.datastorageServices.getCityAPI(countryid)
				.subscribe(
				data => {
					this.cities = this.ObjNgForPipe.transform(data['data']);
					this.vehicle.city_id = (data['Results'][0]['city']) ? data['Results'][0]['city'] : this.vehicle.city_id;
				}
				)
		} 
	}
	
	/**
	 * 
	 * @desc As per the year range, get list of models
	 * @access public
	 * @param yearFrom
	 * @param yearTo
	 */
	setMakes(yearFrom:number, vinData:any){
		let yearFromIdx = this.year_from.find(x => x.name==yearFrom);
		let formData=new FormData();
		formData.append('yearFrom' , yearFromIdx['id']);
		this.datastorageServices.getMakeAPI(formData)
									.subscribe(
										data => {
											if(data.status=='success'){
												this.makes=data['data'];
												if(vinData){
													let makeObj = this.makes.find(x => x.key.toLowerCase()==vinData['Results'][0]['Make'].toLowerCase());
													this.vehicle.make_id=(makeObj)?makeObj.id:'';
													this.getAllModels(this.vehicle.make_id, vinData);
												}
											}
										}
									);
	}

	/**
	 * 
	 * @access public
	 * @param makeid
	 * 
	 * @desc fetches all Models based on the make with its make id passed
	 */
	public getAllModels(makeid:any, vinData:any) {
		if (makeid) {
			let yearFromIdx = this.year_from.find(x => x.name==this.vehicle.yearfrom);				
			let formData=new FormData();
			formData.append('yearFrom' , yearFromIdx['id']);
			formData.append('make_id' , makeid);
			this.datastorageServices.getModelAPI(formData)
				.subscribe(
				data => {
					if(data.status=='success'){
						this.models=data['data'];
						if(vinData){
							let modelObj=this.models.find(x => x.name.toLowerCase() == vinData['Results'][0]['Model'].toLowerCase());
							this.vehicle.model_id = (modelObj)?modelObj.id:'';
							this.getAllTrims(this.vehicle.model_id, vinData);
						}
					}else{
						this.models=[];
					}
				}
				)
		} else {
			this.models = [];
			this.vehicle.model_id ='';
		}
	}

	/**
	 * 
	 * @desc As per the model id, get list of all trims.
	 * @access public
	 * @param model_id
	 */
	public getAllTrims(model_id:any, vinData:any){
		
				let yearFromIdx = this.year_from.find(x => x.name==this.vehicle.yearfrom);				
				let formData=new FormData();
				formData.append('yearFrom' , yearFromIdx['id']);
				formData.append('make_id', this.vehicle.make_id);
				formData.append('model_id', model_id);
				this.datastorageServices.getTrimsAPI(formData)
											.subscribe(
												data => {
													if(data.status=='success'){
														this.trims=data['data'];
														if(vinData){
															let trimObj=this.trims.find(x => x.name.toLowerCase()==vinData['Results'][0]['Trim'].toLowerCase());
															this.vehicle.trim_id=(trimObj)?trimObj.id:'';
														}
													}else{
														this.trims=[];
													}
												}
											);
	}

		/**
	 * 
	 * @desc As per the yera/make/model/trim get list of all other details.
	 * @access public
	 * @param trim_id
	 */
	public getSpecifications(trim_id: any){
		
				let yearFromIdx = this.year_from.find(x => x.name==this.vehicle.yearfrom);				
				let formData=new FormData();
				formData.append('yearFrom' , yearFromIdx['id']);
				formData.append('make_id', this.vehicle.make_id);
				formData.append('model_id', this.vehicle.model_id);
				formData.append('trim_id', trim_id);
				this.datastorageServices.getSpecification(formData)
											.subscribe(
												response => {
													this.vehicle.model_body='';
													this.vehicle.engine_size='';
													this.vehicle.engine_cylinders='';
													this.vehicle.horsepower='';
													this.vehicle.fuel_type='';
													this.vehicle.doors='';
													this.vehicle.seats='';
													if(response.status=='success'){
														this.vehicle.model_body=response.data.model_body_id;
														this.vehicle.engine_size=response.data.model_engine_cc_id;
														this.vehicle.engine_cylinders=response.data.model_engine_cylinders_id;
														this.vehicle.horsepower=response.data.model_engine_power_ps_id;
														this.vehicle.fuel_type=(response.data.model_engine_fuel_id != 0)?response.data.model_engine_fuel_id : '';
														this.vehicle.doors=response.data.model_doors_id;
														let seatsIdx=this.seat.find(x => x.id == response.data.seat_id);
														(seatsIdx) ? this.vehicle.seats = seatsIdx['name'] : ''; 
													}
												}
											);
	}


	scan(){
		let self=this;
		let options = {
			types: {
				Code128: true,
				Code39: true,
				Code93: true,
				CodaBar: true,
				DataMatrix: true,
				EAN13: true,
				EAN8: true,
				ITF: true,
				QRCode: true,
				UPCA: true,
				UPCE: true,
				PDF417: true,
				Aztec: true
			},
			detectorSize: {
				width: .5,
				height: .7
			}
		}
		this.vin.nativeElement.focus();
		// cordova.plugins.barcodeScanner.scan(
		// 	function(result) {
		// 		self.vehicle.vin=result.text;
		// 		self.getVinInfo(result.text);
		// 		self.vin.nativeElement.blur();
		// 		setTimeout(() => {
		// 			self.vin.nativeElement.focus();
		// 		}, 100);
				
		// 		console.log("We got a barcode\n" +
		// 			  "Result: " + result.text + "\n" +
		// 			  "Format: " + result.format + "\n" +
		// 			  "Cancelled: " + result.cancelled+"\n"+
		// 			  "vininfo"+ self.vehicle.vin);
		// 	},
		// 	function(error) {
		// 		self.flashMessagesService.show(error,{classes: ['alert', 'alert-danger'], timeout: 2200});
		// 		console.log("Scanning failed: " + error);
		// 	}
		//  );

		window.plugins.GMVBarcodeScanner.scan(options,function(error, result) { 
    
			//Handle Errors
			if(error) {
				self.flashMessagesService.show(error.message,{classes: ['alert', 'alert-danger'], timeout: 2200});
				console.log("Scanning failed: " , error);	
				return;
			}
			
			//Do something with the data.
			console.log(result);
			self.vehicle.vin=result;
			self.getVinInfo(result);
			self.vin.nativeElement.blur();
			setTimeout(() => {
				self.vin.nativeElement.focus();
			}, 100);
		});

	}
			
}